Install apache2

sudo apt update

sudo apt install apache2

Install mysql

sudo apt update

sudo apt install mysql-server

sudo mysql_secure_installation

Install phpmyadmin 

sudo apt update

sudo apt install phpmyadmin php-mbstring php-gettext

sudo phpenmod mbstring



Installation

Please check the official laravel installation guide for server requirements before you start.

Official Documentation

Clone the repository into your apache's www folder

git clone https://gitlab.com/madaliou/finlobackend.git

Switch to the repo folder

cd finlobackend

Install all the dependencies using composer

composer install

Copy the example env file and make the required configuration changes in the .env file

cp .env.example .env

Generate a new application key

php artisan key:generate

Generate a new JWT authentication secret key

php artisan jwt:generate

Run the database migrations (Set the database connection in .env before migrating)

php artisan migrate

Start the local development server

php artisan serve