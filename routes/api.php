<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/*header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');*/




    Route::middleware('guest')->get('/user', function (Request $request) {
        return $request->user();
       
    });

    /*Route::middleware('guest')->post('authentication',['uses'=>'ApiController@authentication']);

    Route::middleware('guest')->get('users',['uses'=>'QuickbooksController@users']);*/


    //pour les utilisateurs
	
   
    Route::get('updateuser',['uses'=>'UserController@updateuser']);

    Route::post('login', 'UserController@login');

    Route::post('register', 'UserController@register');

    Route::post('insert', 'UserController@insert'); 

    Route::group(['middleware' => 'jwt.verify'], function () {

        Route::get('parameters',['uses'=>'ParameterController@list']);

        Route::get('/parameters/{id}', 'ParameterController@oneParameter');

        Route::post('/parameters', 'ParameterController@create');
        
        Route::put('/parameters/{id}', 'ParameterController@edit');

        Route::delete('/parameters/{id}', 'ParameterController@destroy');
        
        Route::post('users',['uses'=>'UserController@create']);

        Route::post('users/excel',['uses'=>'UserController@createByExcel']);

        Route::get('users',['uses'=>'UserController@list']);

        Route::put('/users/{id}', 'UserController@edit');

        Route::delete('/users/{id}', 'UserController@destroy');
        
        Route::get('logout', 'UserController@logout');
    
        Route::get('user', 'UserController@getAuthUser');

        Route::get('userGames', 'UserController@getAuthUserGames');        

        Route::get('games',['uses'=>'GameController@list']);

        Route::post('searchInGamesList',['uses'=>'GameController@searchInGamesList']);

        Route::post('searchInUsersList',['uses'=>'UserController@searchInUsersList']);
        
        Route::post('games',['uses'=>'GameController@store']);

        Route::get('shuffle',['uses'=>'GameController@shuffle']);
    
        //Route::delete('games/{id}', 'GameController@delete');        

        Route::put('validation/{id}', 'GameController@easyWordValidation');

    
    });




