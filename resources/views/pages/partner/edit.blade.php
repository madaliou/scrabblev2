@extends('layouts.manager')
@section('title','CIGC | Admin')
@section('content')



<div class="app-content content container-fluid">
    <div class="content-wrapper">
      <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Modification de Partenaire</h2>
          </div>
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('partner')}}">Accueil</a>
                </li>
                <li class="breadcrumb-item"><a href="{{url('partner')}}">Utilisateurs</a>
                </li>
                <li class="breadcrumb-item active">Modifier
                </li>
              </ol>
            </div>
      </div>
    </div>
    <div class="content-body">
<!-- Page -->



    <div class="page-content">
      <!-- Panel Form Elements -->
      
        
        
           <div class="col-lg-10">

           {!! Form::model($partner, ['route' => ['partner.update', $partner->id], 'method' => 'put', 'class' => 'form-horizontal ']) !!}
               

                  <div class="form-group">
                  
                        <label class="col-md-3 control-label">Nom </label>
                        
                        <div class="col-md-8">

                          {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'saisir le nom']) !!}


                        {!! $errors->first('name', '<small class="help-block">:message</small>') !!}
                        <br/>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Téléphone</label>
                        <div class="col-md-8">

                        {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'saisir le numero']) !!}

                        {!! $errors->first('phone', '<small class="help-block">:message</small>') !!}
                        <br/>
                        </div>

                    </div>



                    <div class="form-group">
                      <label class="col-md-3 control-label">Adresse</label>
                      <div class="col-md-8">

                      {!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'saisir l\'adresse']) !!}

                      {!! $errors->first('address', '<small class="help-block">:message</small>') !!}
                      <br/>
                      </div>

                  </div>

                  <div class="form-group">
                    <label class="col-md-3 control-label">Pays</label>
                    <div class="col-md-8">

                    {!! Form::text('country', null, ['class' => 'form-control', 'placeholder' => 'saisir le numero']) !!}

                    {!! $errors->first('country', '<small class="help-block">:message</small>') !!}
                    <br/>
                    </div>

                </div>

                <div class="form-group">
                  <label class="col-md-3 control-label">Ville</label>
                  <div class="col-md-8">

                  {!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'saisir le numero']) !!}

                  {!! $errors->first('city', '<small class="help-block">:message</small>') !!}
                  <br/>
                  </div>

              </div>
                
                    <div class="form-group">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6">
                    <br/> <br/>
                    {!! Form::submit('Valider', ['class' => 'btn btn-primary ', 'style' => 'width : 90px;']) !!}
                    <br/> <br/>  <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/>
                    </div>
                  

                       
                 

                  </div>
                </form>
            </div><!-- end col -->

        </div>
      </div>
    
  </div>

 
    </div>
  </div>

@endsection
