@extends('layouts.manager')
@section('title','TransVie | Admin')
@section('content')

<!-- DASHBOARD CONTENT -->

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
    	</div>
        <div class="content-body">
				@if (session('ok'))
			<br/>
			<div role="alert" class="alert alert-success alert-dismissible">
			<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><span class="icon mdi mdi-check"></span>
			<strong></strong> {{ session('ok') }}
			</div>
		@endif
		@if (session('danger'))
		<br/>
		<div role="alert" class="alert alert-danger alert-dismissible">
			<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><span class="icon mdi mdi-check"></span>
			<strong>Erreur!</strong> {{ session('danger') }}
		</div>
		@endif

		<div class="page-header-actions">
			<!-- Button trigger modal -->

			@if(Auth::user()->role == "admin")
			<button type="button" id="ajoutModal" class="btn btn-success mrg-b-lg pull-left" data-toggle="modal" data-target="#iconForm">
			Ajouter
			</button>
			@endif
		</div>

		<div class="col-lg-4 col-md-6 col-sm-12">
			<div class="form-group">						
					<!-- Modal -->
					<div class="modal fade text-xs-left" id="iconForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
								</button>
								<h3 class="modal-title" id="myModalLabel34">Ajout de partenaire</h3>
							</div>
							<form action="{{url('partner')}}" method= "POST">
							@csrf
								<div class="modal-body">
								<label>Nom du partenaire: </label>
									<div class="form-group position-relative has-icon-left">
										<input type="text" placeholder="Nom du partenaire" name="name" class="form-control" required>
										<div class="form-control-position">
											<i class="icon-user4 text-muted"></i>
										</div>

										
									</div>
									<label>Numero de téléphone: </label>
									<div class="form-group position-relative has-icon-left">
										<input type="text" placeholder="Adresse du partenaire" name="phone" class="form-control" required>
										<div class="form-control-position">
											<i class="icon-user4 text-muted"></i>
										</div>
									</div>

									<label>Adresse du partenaire: </label>
									<div class="form-group position-relative has-icon-left">
										<input type="text" placeholder="Adresse du partenaire" name="address" class="form-control" required>
										<div class="form-control-position">
											<i class="icon-user4 text-muted"></i>
										</div>
									</div>
									<label>Choisir </label>
									<div class="form-group position-relative has-icon-left">
										<select class="form-control" data-plugin="select2" name="location">
						
										  <option value="Dakar">Dakar</option>
										  <option value="Hors Dakar">Hors Dakar</option>
						
										</select>
						
						
									</div>

									<label>Ville </label>
									<div class="form-group position-relative has-icon-left">
										<input type="text" placeholder="Pays" name="city" class="form-control" required>
										<div class="form-control-position">
											<i class="icon-user4 text-muted"></i>
										</div>
									</div>


									<label>Pays </label>
									<div class="form-group position-relative has-icon-left">
										<input type="text" placeholder="Ville" name="country" class="form-control" required>
										<div class="form-control-position">
											<i class="icon-user4 text-muted"></i>
										</div>
									</div>									
									
									
								</div>
								<div class="modal-footer">
									<input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Annuler">
									<input type="submit" class="btn btn-outline-primary btn-lg" value="Ok">
								</div>
							</form>
							</div>
						</div>
					</div>
			</div>
		</div>

		<div class="panel">

		<h1>Liste des partenaires</h1> 

			<div class="card-body collapse in">
				<div class="card-block card-dashboard">

					<table class="table table-striped table-bordered dataex-html5-export">
						<thead>
							<tr>
							<th>#</th>
								<th>Nom du partenaire</th>
								<th>Adresse du partenaire</th>
								<th>Téléphone</th>
								<th>Action</th>
								<th>Modifier</th>
								<th>Supprimer</th>							
							</tr>
						</thead>
						<tbody>
							<?php $i=0;?>
							@foreach ($partners as $partner)
							<tr>
							<td> <?php echo $i = $i + 1;?></td>
							<td>{{$partner->name}}</td>
							<td>{{$partner->location}}</td>
							
							<td>{{$partner->phone}}</td>
							<td>

								@if($partner->activate == 0)

									<form method="post" action="activePartner">
									{{ csrf_field() }}
									<input type="hidden" name="partner_id" value="{{$partner->id}}">

										<button type="submit" class="btn btn-warning" onclick = "return confirm(\'Voulez-vous vraiment activer cet utilisateur ?\')">Activer</button>

									</form>
								@else
									
									<form method="post" action="desactivePartner">
									{{ csrf_field() }}
									<input type="hidden" name="partner_id" value="{{$partner->id}}">

										<button type="submit" class="btn btn-danger" onclick = "return confirm(\'Voulez-vous vraiment activer cet utilisateur ?\')">Désact</button>

									</form>
									
								@endif
							</td>
							<td><a href="{{action('PartnerController@edit', $partner['id'])}}" class="btn btn-success">
							<i class="icon-pencil3"></i>
							</a>
												
							</td>

							<td>
							<form action="{{action('PartnerController@destroy', $partner['id'])}}" method="post">
									@csrf
									<input name="_method" type="hidden" value="DELETE">
									<button class="btn btn-danger" type="submit"
									onclick="return confirm('Voulez-vous vraiment supprimer ?');"><i class="icon-trash-o"></i></button>
								</form>
							</td>

							</tr>
							@endforeach								
						</tbody>
						<tfoot>
							<tr>
							<th>#</th>
							<th>Nom du partenaire</th>
								<th>Age du partenaire</th>
														
		
								<th>Modifier</th>
								<th>Supprimer</th>
							</tr>
						</tfoot>
					</table>

				</div>
				</div>

			</div>


		</div>
	</div>					
</div>


@stop

