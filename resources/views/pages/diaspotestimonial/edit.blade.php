@extends('layouts.manager')
@section('title','CIGC | Admin')
@section('content')



<div class="app-content content container-fluid">
    <div class="content-wrapper">
      <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Modification de Témoignage</h2>
          </div>
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('diaspotestimonial')}}">Accueil</a>
                </li>
                <li class="breadcrumb-item"><a href="{{url('diaspotestimonial')}}">Utilisateurs</a>
                </li>
                <li class="breadcrumb-item active">Modifier
                </li>
              </ol>
            </div>
      </div>
    </div>
    <div class="content-body">
<!-- Page -->



    <div class="page-content">
      <!-- Panel Form Elements -->
      
        
        
           <div class="col-lg-10">

           {!! Form::model($diaspotestimonial, ['route' => ['diaspotestimonial.update', $diaspotestimonial->id], 'method' => 'put', 'class' => 'form-horizontal ']) !!}
               

                  <div class="form-group">
                  
                        <label class="col-md-3 control-label">Nom et prénoms </label>
                        
                        <div class="col-md-8">

                          {!! Form::text('author_name', null, ['class' => 'form-control', 'placeholder' => 'saisir le nom']) !!}


                        {!! $errors->first('author_name', '<small class="help-block">:message</small>') !!}
                        <br/>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Age de l'auteur</label>
                        <div class="col-md-8">

                        {!! Form::text('author_age', null, ['class' => 'form-control', 'placeholder' => 'saisir le nom']) !!}

                       

                        {!! $errors->first('author_age', '<small class="help-block">:message</small>') !!}
                        <br/>
                        </div>

                    </div>

                     <div class="form-group">
                        <label class="col-md-3 control-label">Adresse de l'auteur</label>
                        <div class="col-md-8">

                        {!! Form::text('author_location', null, ['class' => 'form-control', 'placeholder' => 'saisir l\'adresse']) !!}

                       

                        {!! $errors->first('author_location', '<small class="help-block">:message</small>') !!}
                        <br/>
                        </div>

                    </div>



                     <div class="form-group">
                        <label class="col-md-3 control-label">Contenu du message</label>
                        <div class="col-md-8">
                        {!! Form::textarea('author_message', null, ['class' => 'form-control', 'placeholder' => 'saisir le nom']) !!}

                        {!! $errors->first('author_message', '<small class="help-block">:message</small>') !!}
                        <br/>
                        </div>
                    </div>                     
                    <div class="form-group">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6">
                    <br/> <br/>
                    {!! Form::submit('Valider', ['class' => 'btn btn-primary ', 'style' => 'width : 90px;']) !!}
                    <br/> <br/>  <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/>
                    </div>
                  

                       
                 

                  </div>
                </form>
            </div><!-- end col -->

        </div>
      </div>
    
  </div>

 
    </div>
  </div>

@endsection
