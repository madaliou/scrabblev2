@extends('layouts.manager')
@section('title','CIGC | Admin')
@section('content')

<!-- DASHBOARD CONTENT -->

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
    	</div>
        <div class="content-body">
				@if (session('ok'))
			<br/>
			<div role="alert" class="alert alert-success alert-dismissible">
			<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><span class="icon mdi mdi-check"></span>
			<strong></strong> {{ session('ok') }}
			</div>
		@endif
		@if (session('danger'))
		<br/>
		<div role="alert" class="alert alert-danger alert-dismissible">
			<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><span class="icon mdi mdi-check"></span>
			<strong>Erreur!</strong> {{ session('danger') }}
		</div>
		@endif

		<div class="page-header-actions">
			<!-- Button trigger modal -->

			@if(Auth::user()->role == "admin")
			<button type="button" id="ajoutModal" class="btn btn-success mrg-b-lg pull-left" data-toggle="modal" data-target="#iconForm">
			Ajouter
			</button>
			@endif
		</div>

		<div class="col-lg-4 col-md-6 col-sm-12">
			<div class="form-group">						
					<!-- Modal -->
					<div class="modal fade text-xs-left" id="iconForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
								</button>
								<h3 class="modal-title" id="myModalLabel34">Ajout réseau</h3>
							</div>
							<form action="{{url('network')}}" method= "POST" enctype="multipart/form-data">
							@csrf
								<div class="modal-body">
								<label>Nom: </label>
									<div class="form-group position-relative has-icon-left">
										<input type="text" placeholder="Nom" name="name" class="form-control" required>
										<div class="form-control-position">
											<i class="icon-user4 text-muted"></i>
										</div>

										
									</div>
									<label>Logo: </label>
									<div class="form-group position-relative has-icon-left">
											<input type="file" class="form-control" name="picture" placeholder="Image de l'entreprise" accept = 'image/jpeg , image/jpg, image/png' required>
										<div class="form-control-position">
											<i class="icon-user4 text-muted"></i>
										</div>
									</div>
									
								</div>
								<div class="modal-footer">
									<input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Annuler">
									<input type="submit" class="btn btn-outline-primary btn-lg" value="Ok">
								</div>
							</form>
							</div>
						</div>
					</div>
			</div>
		</div>

		<div class="panel">

		<h1>Liste du réseau</h1> 

			<div class="card-body collapse in">
				<div class="card-block card-dashboard">

					<table class="table table-striped table-bordered dataex-html5-export">
						<thead>
							<tr>
							<th>#</th>
								<th>Nom</th>
								<th>Image</th>
								<th>Activation</th>
								
								
								<th>Modifier</th>
								<th>Supprimer</th>							
							</tr>
						</thead>
						<tbody>
							<?php $i=0;?>
							@foreach ($networks as $network)
							<tr>
							<td> <?php echo $i = $i + 1;?></td>
							<td>{{$network->name}}</td>
							<td>
									<span class="avatar avatar-100 pull-left margin-right-20"><img src="{{URL::to('uploads')}}/{{$network->picture}}"></span>
							</td>					
							
							
							<td>

								@if($network->activate == 0)

									<form method="post" action="activeNetwork">
									{{ csrf_field() }}
									<input type="hidden" name="network_id" value="{{$network->id}}">

										<button type="submit" class="btn btn-warning" onclick = "return confirm(\'Voulez-vous vraiment activer cet utilisateur ?\')">Activer</button>

									</form>
								@else
									
									<form method="post" action="desactiveNetwork">
									{{ csrf_field() }}
									<input type="hidden" name="network_id" value="{{$network->id}}">

										<button type="submit" class="btn btn-danger" onclick = "return confirm(\'Voulez-vous vraiment activer cet utilisateur ?\')">Désact</button>

									</form>
									
								@endif
							</td>

							<td><a href="{{action('NetworkController@edit', $network['id'])}}" class="btn btn-success">
							<i class="icon-pencil3"></i>
							</a>
												
							</td>

							<td>
							<form action="{{action('NetworkController@destroy', $network['id'])}}" method="post">
									@csrf
									<input name="_method" type="hidden" value="DELETE">
									<button class="btn btn-danger" type="submit"
									onclick="return confirm('Voulez-vous vraiment supprimer ?');"><i class="icon-trash-o"></i></button>
								</form>
							</td>

							</tr>
							@endforeach								
						</tbody>
						<tfoot>
							<tr>
							<th>#</th>
							<th>Nom</th>
								<th>Image</th>
								<th>Activation</th>
								
								<th>Modifier</th>
								<th>Supprimer</th>
							</tr>
						</tfoot>
					</table>

				</div>
				</div>

			</div>


		</div>
	</div>					
</div>


@stop

