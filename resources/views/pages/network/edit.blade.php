@extends('layouts.manager')
@section('title','CIGC | Admin')
@section('content')



<div class="app-content content container-fluid">
    <div class="content-wrapper">
      <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Modification Réseau</h2>
          </div>
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('network')}}">Accueil</a>
                </li>
                <li class="breadcrumb-item"><a href="{{url('network')}}">Utilisateurs</a>
                </li>
                <li class="breadcrumb-item active">Modifier
                </li>
              </ol>
            </div>
      </div>
    </div>
    <div class="content-body">
<!-- Page -->



    <div class="page-content">
      <!-- Panel Form Elements -->
      
        
        
           <div class="col-lg-6">

           {!! Form::model($network, ['route' => ['network.update', $network->id], 'method' => 'put', 'class' => 'form-horizontal', 'files'=>true]) !!}
               

                  <div class="form-group">
                  
                        <label class="col-md-2 control-label">Nom</label>
                        
                        <div class="col-md-10">

                          {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'saisir le nom']) !!}


                        {!! $errors->first('name', '<small class="help-block">:message</small>') !!}
                        <br/>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Image</label>
                        <div class="col-md-8">
                            
                            <a class="avatar avatar-100 pull-left margin-right-20" href="javascript:void(0)">
                              @if($network->picture != " ")
                              
                              <img src="{{URL::to('uploads')}}/{{$network->picture}}" alt="...">
                              @else 
                              
                              <img src="{{asset ("admin/global/portraits/user.png")}}" alt="...">
                              
                              @endif
 
                            </a>   
                            
                              <input type="file" class="form-control" name="picture"  class="form-control"
                                  placeholder=" LOGO" value="{{$network->picture}}" accept = 'image/jpeg , image/jpg, image/png' >
                                  {!! $errors->first('picture', '<small class="help-block">:message</small>') !!}
                                  
                          </div>
                    </div>

                                  
                    <div class="form-group">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6">
                    <br/> <br/>
                    {!! Form::submit('Valider', ['class' => 'btn btn-primary ', 'style' => 'width : 90px;']) !!}
                    <br/> <br/>  <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/>
                    </div>
                  

                       
                 

                  </div>
                </form>
            </div><!-- end col -->

        </div>
      </div>
    
  </div>

 
    </div>
  </div>

@endsection
