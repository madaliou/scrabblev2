@extends('layouts.manager')
@section('title','TransVie | Admin')
@section('content')

<!-- DASHBOARD CONTENT -->

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
    	</div>
        <div class="content-body">
				@if (session('ok'))
			<br/>
			<div role="alert" class="alert alert-success alert-dismissible">
			<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><span class="icon mdi mdi-check"></span>
			<strong></strong> {{ session('ok') }}
			</div>
		@endif
		@if (session('danger'))
		<br/>
		<div role="alert" class="alert alert-danger alert-dismissible">
			<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><span class="icon mdi mdi-check"></span>
			<strong>Erreur!</strong> {{ session('danger') }}
		</div>
		@endif

		<div class="page-header-actions">
			<!-- Button trigger modal -->

			@if(Auth::user()->role == "admin")
			<button type="button" id="ajoutModal" class="btn btn-success mrg-b-lg pull-left" data-toggle="modal" data-target="#iconForm">
			Ajouter
			</button>
			@endif
		</div>

		<div class="col-lg-4 col-md-6 col-sm-12">
			<div class="form-group">						
					<!-- Modal -->
					<div class="modal fade text-xs-left" id="iconForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
								</button>
								<h3 class="modal-title" id="myModalLabel34">Ajout de témoignage</h3>
							</div>
							<form action="{{url('testimonial')}}" method= "POST">
							@csrf
								<div class="modal-body">
								<label>Nom et prénom(s): </label>
									<div class="form-group position-relative has-icon-left">
										<input type="text" placeholder="Nom et prénom(s)" name="author_name" class="form-control" required>
										<div class="form-control-position">
											<i class="icon-user4 text-muted"></i>
										</div>

										
									</div>
									<label>Age de l'auteur: </label>
									<div class="form-group position-relative has-icon-left">
										<input type="text" placeholder="Age de l'auteur" name="author_age" class="form-control" required>
										<div class="form-control-position">
											<i class="icon-user4 text-muted"></i>
										</div>
									</div>
									<label>Adresse de l'auteur: </label>
									<div class="form-group position-relative has-icon-left">
										<input type="text" placeholder="Adresse de l'auteur" name="author_location" class="form-control" required>
										<div class="form-control-position">
											<i class="icon-mail6 text-muted"></i>
										</div>
										@if ($errors->has('author_location'))
										<span class="help-block">
											<strong style="color:rgb(255,0,40);">{{ $errors->first('author_location') }}</strong>
										</span>
										@endif
									</div>
									<label>Contenu du message: </label>
									<div class="form-group position-relative has-icon-left">
										<textarea name="author_message" rows="5" cols="68"></textarea>
										
										
										@if ($errors->has('email'))
										<span class="help-block">
											<strong style="color:rgb(255,0,40);">{{ $errors->first('author_message') }}</strong>
										</span>
										@endif
									</div>
									
								</div>
								<div class="modal-footer">
									<input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Annuler">
									<input type="submit" class="btn btn-outline-primary btn-lg" value="Ok">
								</div>
							</form>
							</div>
						</div>
					</div>
			</div>
		</div>

		<div class="panel">
		<h1>Liste des témoignages</h1> 
			<div class="card-body collapse in">
				<div class="card-block card-dashboard">

					<table class="table table-striped table-bordered dataex-html5-export">
						<thead>
							<tr>
							
								<th>Nom de l'auteur</th>
								
								<th>Adresse de l'auteur</th>
								<th>Contenu du message</th>
								<th>Activation</th>
		
								<th>Modifier</th>
								<th>Supprimer</th>							
							</tr>
						</thead>
						<tbody>
							
							@foreach ($testimonials as $testimonial)
							<tr>
							
							<td>{{$testimonial->author_name}}</td>
							<!--td>{{$testimonial->author_age}}</td-->
							<td>{{$testimonial->author_location}}</td>
							<td>{{$testimonial->author_message}}</td>
							<td>

								@if($testimonial->activate == 0)

									<form method="post" action="activeTestimonial">
									{{ csrf_field() }}
									<input type="hidden" name="testimonial_id" value="{{$testimonial->id}}">

										<button type="submit" class="btn btn-warning" onclick = "return confirm(\'Voulez-vous vraiment activer cet utilisateur ?\')">Activer</button>

									</form>
								@else
									
									<form method="post" action="desactiveTestimonial">
									{{ csrf_field() }}
									<input type="hidden" name="testimonial_id" value="{{$testimonial->id}}">

										<button type="submit" class="btn btn-danger" onclick = "return confirm(\'Voulez-vous vraiment activer cet utilisateur ?\')">Désact</button>

									</form>
									
								@endif
							</td>

							<td><a href="{{action('TestimonialController@edit', $testimonial['id'])}}" class="btn btn-success">
							<i class="icon-pencil3"></i>
							</a>
												
							</td>

							<td>
							<form action="{{action('TestimonialController@destroy', $testimonial['id'])}}" method="post">
									@csrf
									<input name="_method" type="hidden" value="DELETE">
									<button class="btn btn-danger" type="submit"
									onclick="return confirm('Voulez-vous vraiment supprimer ?');"><i class="icon-trash-o"></i></button>
								</form>
							</td>

							</tr>
							@endforeach								
						</tbody>
						<tfoot>
							<tr>
							
							<th>Nom de l'auteur</th>
								
								<th>Adresse de l'auteur</th>
								<th>Contenu du message</th>
								<th>Activation</th>
		
								<th>Modifier</th>
								<th>Supprimer</th>
							</tr>
						</tfoot>
					</table>

				</div>
				</div>

			</div>


		</div>
	</div>					
</div>


@stop

