@extends('layouts.manager')
@section('title','CIGC | Admin')
@section('content')



<div class="app-content content container-fluid">
    <div class="content-wrapper">
      <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Modification de Prix</h2>
          </div>
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('diasporaprice')}}">Accueil</a>
                </li>
                <li class="breadcrumb-item"><a href="{{url('diasporaprice')}}">Utilisateurs</a>
                </li>
                <li class="breadcrumb-item active">Modifier
                </li>
              </ol>
            </div>
      </div>
    </div>
    <div class="content-body">
<!-- Page -->



    <div class="page-content">
      <!-- Panel Form Elements -->
      
        
        
           <div class="col-lg-10">

           {!! Form::model($diasporaprice, ['route' => ['diasporaprice.update', $diasporaprice->id], 'method' => 'put', 'class' => 'form-horizontal ']) !!}
               

                  <div class="form-group">
                  
                        <label class="col-md-3 control-label">Prestige </label>
                        
                        <div class="col-md-8">

                          {!! Form::text('prestige', null, ['class' => 'form-control', 'placeholder' => 'prestige']) !!}


                        {!! $errors->first('prestige', '<small class="help-block">:message</small>') !!}
                        <br/>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Confort</label>
                        <div class="col-md-8">

                        {!! Form::text('confort', null, ['class' => 'form-control', 'placeholder' => 'confort']) !!}

                       

                        {!! $errors->first('confort', '<small class="help-block">:message</small>') !!}
                        <br/>
                        </div>

                    </div>

                     <div class="form-group">
                        <label class="col-md-3 control-label">Medium</label>
                        <div class="col-md-8">

                        {!! Form::text('medium', null, ['class' => 'form-control', 'placeholder' => 'medium']) !!}

                       

                        {!! $errors->first('medium', '<small class="help-block">:message</small>') !!}
                        <br/>
                        </div>

                    </div>



                     <div class="form-group">
                        <label class="col-md-3 control-label">Classic</label>
                        <div class="col-md-8">
                        {!! Form::text('classic', null, ['class' => 'form-control', 'placeholder' => 'classic']) !!}

                        {!! $errors->first('classic', '<small class="help-block">:message</small>') !!}
                        <br/>
                        </div>
                    </div>   

                    <div class="form-group">
                        <label class="col-md-3 control-label">Xeweul</label>
                        <div class="col-md-8">
                        {!! Form::text('xeweul', null, ['class' => 'form-control', 'placeholder' =>'xeweul']) !!}

                        {!! $errors->first('xeweul', '<small class="help-block">:message</small>') !!}
                        <br/>
                        </div>
                    </div>   


                    <div class="form-group">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6">
                    <br/> <br/>
                    {!! Form::submit('Valider', ['class' => 'btn btn-primary ', 'style' => 'width : 90px;']) !!}
                    <br/> <br/>  <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/>
                    </div>
                  

                       
                 

                  </div>
                </form>
            </div><!-- end col -->

        </div>
      </div>
    
  </div>

 
    </div>
  </div>

@endsection
