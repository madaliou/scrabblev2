@extends('layouts.manager')
@section('title','TransVie | Admin')
@section('content')

<!-- DASHBOARD CONTENT -->

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
    	</div>
        <div class="content-body">
				@if (session('ok'))
			<br/>
			<div role="alert" class="alert alert-success alert-dismissible">
			<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><span class="icon mdi mdi-check"></span>
			<strong></strong> {{ session('ok') }}
			</div>
		@endif
		@if (session('danger'))
		<br/>
		<div role="alert" class="alert alert-danger alert-dismissible">
			<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><span class="icon mdi mdi-check"></span>
			<strong>Erreur!</strong> {{ session('danger') }}
		</div>
		@endif

		<div class="page-header-actions">
			<!-- Button trigger modal -->

			@if(Auth::user()->role == "admin")
			<button type="button" id="ajoutModal" class="btn btn-success mrg-b-lg pull-left" data-toggle="modal" data-target="#iconForm">
			Ajouter
			</button>
			@endif
		</div>

		<div class="col-lg-4 col-md-6 col-sm-12">
			<div class="form-group">						
					<!-- Modal -->
					<div class="modal fade text-xs-left" id="iconForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
								</button>
								<h3 class="modal-title" id="myModalLabel34">Ajout de prix</h3>
							</div>
							<form action="{{url('diasporaprice')}}" method= "POST">
							@csrf
								<div class="modal-body">
								<label>Prestige:</label>
									<div class="form-group position-relative has-icon-left">
										<input type="text" placeholder="Prestige" name="prestige" class="form-control" required>
										
									</div>
									<label>Confort: </label>
									<div class="form-group position-relative has-icon-left">
										<input type="text" placeholder="Confort" name="confort" class="form-control" required>
										
									</div>
									<label>Medium: </label>
									<div class="form-group position-relative has-icon-left">
										<input type="text" placeholder="Medium" name="medium" class="form-control" required>
										
										@if ($errors->has('medium'))
										<span class="help-block">
											<strong style="color:rgb(255,0,40);">{{ $errors->first('medium') }}</strong>
										</span>
										@endif
									</div>
									<label>Classique </label>
									<div class="form-group position-relative has-icon-left">
									<input type="text" placeholder="Medium" name="classic" class="form-control" required>
																		
										@if ($errors->has('classic'))
										<span class="help-block">
											<strong style="color:rgb(255,0,40);">{{ $errors->first('classic') }}</strong>
										</span>
										@endif
									</div>
									<label>Xeweul: </label>
									<div class="form-group position-relative has-icon-left">
										<input type="text" placeholder="xeweul" name="xeweul" class="form-control" required>
										
										@if ($errors->has('xeweul'))
										<span class="help-block">
											<strong style="color:rgb(255,0,40);">{{ $errors->first('xeweul') }}</strong>
										</span>
										@endif
									</div>
									
								</div>
								<div class="modal-footer">
									<input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Annuler">
									<input type="submit" class="btn btn-outline-primary btn-lg" value="Ok">
								</div>
							</form>
							</div>
						</div>
					</div>
			</div>
		</div>

		<div class="panel">
		<h1>Prix de la diaspora</h1> 
			<div class="card-body collapse in">
				<div class="card-block card-dashboard">

					<table class="table table-striped table-bordered dataex-html5-export">
						<thead>
							<tr>
							
								<th>Prestige</th>
								
								<th>Confort</th>
								<th>Medium</th>
								<th>Classique</th>
								<th>Xeweul</th>
								
		
								<th>Modifier</th>
								<th>Supprimer</th>							
							</tr>
						</thead>
						<tbody>
							
							@foreach ($diasporaprices as $diasporaprice)
							<tr>
							
							<td>{{$diasporaprice->prestige}}</td>
							<td>{{$diasporaprice->confort}}</td>
							<td>{{$diasporaprice->medium}}</td>
							<td>{{$diasporaprice->classic}}</td>
							<td>{{$diasporaprice->xeweul}}</td>
							

							<td><a href="{{action('DiasporapriceController@edit', $diasporaprice['id'])}}" class="btn btn-success">
							<i class="icon-pencil3"></i>
							</a>
												
							</td>

							<td>
							<form action="{{action('DiasporapriceController@destroy', $diasporaprice['id'])}}" method="post">
									@csrf
									<input name="_method" type="hidden" value="DELETE">
									<button class="btn btn-danger" type="submit"
									onclick="return confirm('Voulez-vous vraiment supprimer ?');"><i class="icon-trash-o"></i></button>
								</form>
							</td>

							</tr>
							@endforeach								
						</tbody>
						<tfoot>
							<tr>
							
							<th>Nom de l'auteur</th>
								
								<th>Confort</th>
								<th>Medium</th>
								
								<th>Classique</th>
								<th>Xeweul</th>
								
								<th>Modifier</th>
								<th>Supprimer</th>
							</tr>
						</tfoot>
					</table>

				</div>
				</div>

			</div>


		</div>
	</div>					
</div>


@stop

