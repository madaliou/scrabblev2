<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <meta name="_token" content="{{csrf_token()}}">
    <title>CIGC</title>
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset ("admin/app-assets/images/ico/apple-icon-60.png") }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset ("admin/app-assets/images/ico/apple-icon-76.png") }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset ("admin/app-assets/images/ico/apple-icon-120.png") }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset ("admin/app-assets/images/ico/apple-icon-152.png") }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset ("admin/app-assets/images/ico/favicon.ico") }}">
    <link rel="shortcut icon" type="image/png" href="{{asset ("admin/app-assets/images/ico/favicon-32.png") }}">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/bootstrap.css") }}">
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/fonts/icomoon.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/fonts/flag-icon-css/css/flag-icon.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/vendors/css/sliders/slick/slick.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/vendors/css/extensions/pace.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/vendors/css/charts/jquery-jvectormap-2.0.3.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/vendors/css/charts/morris.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/vendors/css/extensions/unslider.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/vendors/css/weather-icons/climacons.min.css") }}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN ROBUST CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/bootstrap-extended.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/app.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/colors.css") }}">
    <!-- END ROBUST CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/core/menu/menu-types/vertical-menu.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/core/colors/palette-gradient.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/plugins/calendars/clndr.css") }}">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/assets/css/style.css") }}">
    <!-- END Custom CSS-->

    <style>
#countup {
    text-align: center;
}
#countup p {
    display: inline-block;
    padding: 10px;
    background: #151515;
    margin: 0 0 20px;
    border-radius: 3px;
    color: white;
    min-width: 2.6rem;
}
</style>
  </head>
  <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

    <!-- navbar-fixed-top-->
    <nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-semi-dark navbar-shadow">
      <div class="navbar-wrapper">
        <div class="navbar-header">
          <ul class="nav navbar-nav">
            <li class="nav-item mobile-menu hidden-md-up float-xs-left"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5 font-large-1"></i></a></li>
             <!--li class="nav-item"><a href="index.html"
             class="navbar-brand nav-link"><img alt="branding logo"
              src="{{asset ("admin/app-assets/images/logo/robust-logo-light.png") }}"
             data-expand="{{asset ("admin/app-assets/images/logo/robust-logo-light.png") }}"
              data-collapse="{{asset ("admin/app-assets/images/logo/robust-logo-small.png") }}"
               class="brand-logo"></a></li-->
               <span class="font-large-2 text-bold-300 success">CIGC</span>
            <li class="nav-item hidden-md-up float-xs-right"><a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="icon-ellipsis pe-2x icon-icon-rotate-right-right"></i></a></li>
          </ul>
        </div>
        <div class="navbar-container content container-fluid">
          <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
            <ul class="nav navbar-nav">
              <li class="nav-item hidden-sm-down"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5"></i></a></li>
              <li class="nav-item nav-search"><a href="#" class="nav-link nav-link-search fullscreen-search-btn"><i class="ficon icon-search7"></i></a></li>
              <li class="nav-item hidden-sm-down"><a href="#" class="nav-link nav-link-expand"><i class="ficon icon-expand2"></i></a></li>
              <!--li class="dropdown nav-item mega-dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link">Mega</a>
                <ul class="mega-dropdown-menu dropdown-menu row">
                  <li class="col-md-2">
                    <h6 class="dropdown-menu-header text-uppercase mb-1"><i class="icon-paper"></i> News</h6>
                    <div id="mega-menu-carousel-example" class="responsive-slick">
                      <div><img src="{{asset ("admin/app-assets/images/slider/slider-2.png") }}" alt="First slide" class="rounded img-fluid mb-1"><a href="#" class="news-title mb-0">Poster Frame PSD</a>
                        <p class="news-content"><span class="font-small-2">January 26, 2016</span></p>
                      </div>
                      <div><img src="{{asset ("admin/app-assets/images/slider/slider-5.png") }}" alt="First slide" class="rounded img-fluid mb-1"><a href="#" class="news-title mt-1 mb-0">Header MockUp</a>
                        <p class="news-content"><span class="font-small-2">January 15, 2016</span></p>
                      </div>
                      <div><img src="{{asset ("admin/app-assets/images/slider/slider-6.png") }}" alt="First slide" class="rounded img-fluid mb-1"><a href="#" class="news-title mt-1 mb-0">2 Poster PSD</a>
                        <p class="news-content"><span class="font-small-2">January 15, 2016</span></p>
                      </div>
                    </div>
                  </li>
                  <li class="col-md-3">
                    <h6 class="dropdown-menu-header text-uppercase"><i class="icon-shuffle3"></i> Drill down menu</h6>
                    <ul class="drilldown-menu">
                      <li class="menu-list">
                        <ul>
                          <li><a href="layout-2-columns.html" class="dropdown-item"><i class="icon-layout"></i> Page layouts & Templates</a></li>
                          <li><a href="#"><i class="icon-layers"></i> Multi level menu</a>
                            <ul>
                              <li><a href="#" class="dropdown-item"><i class="icon-share4"></i>  Second level</a></li>
                              <li><a href="#"><i class="icon-umbrella3"></i> Second level menu</a>
                                <ul>
                                  <li><a href="#" class="dropdown-item"><i class="icon-microphone2"></i>  Third level</a></li>
                                  <li><a href="#" class="dropdown-item"><i class="icon-head"></i> Third level</a></li>
                                  <li><a href="#" class="dropdown-item"><i class="icon-signal2"></i> Third level</a></li>
                                  <li><a href="#" class="dropdown-item"><i class="icon-camera8"></i> Third level</a></li>
                                </ul>
                              </li>
                              <li><a href="#" class="dropdown-item"><i class="icon-flag4"></i> Second level, third link</a></li>
                              <li><a href="#" class="dropdown-item"><i class="icon-box"></i> Second level, fourth link</a></li>
                            </ul>
                          </li>
                          <li><a href="color-palette-primary.html" class="dropdown-item"><i class="icon-marquee-plus"></i> Color pallet system</a></li>
                          <li><a href="sk-2-columns.html" class="dropdown-item"><i class="icon-edit2"></i> Page starter kit</a></li>
                          <li><a href="changelog.html" class="dropdown-item"><i class="icon-files-empty"></i> Change log</a></li>
                          <li><a href="http://support.pixinvent.com/" class="dropdown-item"><i class="icon-tencent-weibo"></i> Customer support center</a></li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                  <li class="col-md-3">
                    <h6 class="dropdown-menu-header text-uppercase"><i class="icon-list2"></i> Accordion</h6>
                    <div id="accordionWrap" role="tablist" aria-multiselectable="true">
                      <div class="card no-border box-shadow-0 collapse-icon accordion-icon-rotate">
                        <div id="headingOne" role="tab" class="card-header p-0 pb-1 no-border"><a data-toggle="collapse" data-parent="#accordionWrap" href="#accordionOne" aria-expanded="true" aria-controls="accordionOne" class="card-title">Accordion Group Item #1</a></div>
                        <div id="accordionOne" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true" class="card-collapse collapse in">
                          <div class="card-body">
                            <p class="accordion-text">Caramels dessert chocolate cake pastry jujubes bonbon. Jelly wafer jelly beans. Caramels chocolate cake liquorice cake wafer jelly beans croissant apple pie.</p>
                          </div>
                        </div>
                        <div id="headingTwo" role="tab" class="card-header p-0 pb-1 no-border"><a data-toggle="collapse" data-parent="#accordionWrap" href="#accordionTwo" aria-expanded="false" aria-controls="accordionTwo" class="card-title collapsed">Accordion Group Item #2</a></div>
                        <div id="accordionTwo" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" class="card-collapse collapse">
                          <div class="card-body">
                            <p class="accordion-text">Sugar plum bear claw oat cake chocolate jelly tiramisu dessert pie. Tiramisu macaroon muffin jelly marshmallow cake. Pastry oat cake chupa chups.</p>
                          </div>
                        </div>
                        <div id="headingThree" role="tab" class="card-header p-0 pb-1 no-border"><a data-toggle="collapse" data-parent="#accordionWrap" href="#accordionThree" aria-expanded="false" aria-controls="accordionThree" class="card-title collapsed">Accordion Group Item #3</a></div>
                        <div id="accordionThree" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false" class="card-collapse collapse">
                          <div class="card-body">
                            <p class="accordion-text">Candy cupcake sugar plum oat cake wafer marzipan jujubes lollipop macaroon. Cake dragée jujubes donut chocolate bar chocolate cake cupcake chocolate topping.</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="col-md-4">
                    <h6 class="dropdown-menu-header text-uppercase mb-1"><i class="icon-mail6"></i> Contact Us</h6>
                    <form>
                      <fieldset class="form-group position-relative has-icon-left">
                        <label for="inputName1" class="col-sm-3 form-control-label">Name</label>
                        <div class="col-sm-9">
                          <input type="text" id="inputName1" placeholder="John Doe" class="form-control">
                          <div class="form-control-position"><i class="icon-head"></i></div>
                        </div>
                      </fieldset>
                      <fieldset class="form-group position-relative has-icon-left">
                        <label for="inputEmail1" class="col-sm-3 form-control-label">Email</label>
                        <div class="col-sm-9">
                          <input type="email" id="inputEmail1" placeholder="john@example.com" class="form-control">
                          <div class="form-control-position"><i class="icon-mail6"></i></div>
                        </div>
                      </fieldset>
                      <fieldset class="form-group position-relative has-icon-left">
                        <label for="inputMessage1" class="col-sm-3 form-control-label">Message</label>
                        <div class="col-sm-9">
                          <textarea id="inputMessage1" rows="2" placeholder="Simple Textarea" class="form-control"></textarea>
                          <div class="form-control-position"><i class="icon-file-text"></i></div>
                        </div>
                      </fieldset>
                      <div class="col-sm-12 mb-1">
                        <button type="button" class="btn btn-primary float-xs-right"><i class="icon-send-o"></i> Send</button>
                      </div>
                    </form>
                  </li>
                </ul>
              </li-->
            </ul>
            <ul class="nav navbar-nav float-xs-right">
              <li class="dropdown dropdown-language nav-item">
              <a id="dropdown-flag" href="{{url('easy')}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link"><i class="icon-gear"></i>
              <span class="selected-language">Facile</span></a>
                <div aria-labelledby="dropdown-flag" class="dropdown-menu">
                <a href="{{url('easy')}}" class="dropdown-item"><!--i class="flag-icon flag-icon-gb"></i--> Facile</a>
                <a href="{{url('normal')}}" class="dropdown-item"><!--i class="flag-icon flag-icon-gb"></i--> Normal</a>
                <a href="{{url('hard')}}" class="dropdown-item"><!--i class="flag-icon flag-icon-fr"></i--> Difficile</a>
                  <!--a href="#" class="dropdown-item"><i class="flag-icon flag-icon-cn"></i> Chinese</a>
                  <a href="#" class="dropdown-item"><i class="flag-icon flag-icon-de"></i> German</a-->
                </div>
              </li>
              <!--li class="dropdown dropdown-notification nav-item"><a href="#" data-toggle="dropdown" class="nav-link nav-link-label"><i class="ficon icon-bell4"></i><span class="tag tag-pill tag-default tag-danger tag-default tag-up">5</span></a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                  <li class="dropdown-menu-header">
                    <h6 class="dropdown-header m-0"><span class="grey darken-2">Notifications</span><span class="notification-tag tag tag-default tag-danger float-xs-right m-0">5 New</span></h6>
                  </li>
                  <li class="list-group scrollable-container"><a href="javascript:void(0)" class="list-group-item">
                      <div class="media">
                        <div class="media-left valign-middle"><i class="icon-cart3 icon-bg-circle bg-cyan"></i></div>
                        <div class="media-body">
                          <h6 class="media-heading">You have new order!</h6>
                          <p class="notification-text font-small-3 text-muted">Lorem ipsum dolor sit amet, consectetuer elit.</p><small>
                            <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">30 minutes ago</time></small>
                        </div>
                      </div></a><a href="javascript:void(0)" class="list-group-item">
                      <div class="media">
                        <div class="media-left valign-middle"><i class="icon-monitor3 icon-bg-circle bg-red bg-darken-1"></i></div>
                        <div class="media-body">
                          <h6 class="media-heading red darken-1">99% Server load</h6>
                          <p class="notification-text font-small-3 text-muted">Aliquam tincidunt mauris eu risus.</p><small>
                            <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Five hour ago</time></small>
                        </div>
                      </div></a><a href="javascript:void(0)" class="list-group-item">
                      <div class="media">
                        <div class="media-left valign-middle"><i class="icon-server2 icon-bg-circle bg-yellow bg-darken-3"></i></div>
                        <div class="media-body">
                          <h6 class="media-heading yellow darken-3">Warning notifixation</h6>
                          <p class="notification-text font-small-3 text-muted">Vestibulum auctor dapibus neque.</p><small>
                            <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Today</time></small>
                        </div>
                      </div></a><a href="javascript:void(0)" class="list-group-item">
                      <div class="media">
                        <div class="media-left valign-middle"><i class="icon-check2 icon-bg-circle bg-green bg-accent-3"></i></div>
                        <div class="media-body">
                          <h6 class="media-heading">Complete the task</h6><small>
                            <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Last week</time></small>
                        </div>
                      </div></a><a href="javascript:void(0)" class="list-group-item">
                      <div class="media">
                        <div class="media-left valign-middle"><i class="icon-bar-graph-2 icon-bg-circle bg-teal"></i></div>
                        <div class="media-body">
                          <h6 class="media-heading">Generate monthly report</h6><small>
                            <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Last month</time></small>
                        </div>
                      </div></a></li>
                  <li class="dropdown-menu-footer"><a href="javascript:void(0)" class="dropdown-item text-muted text-xs-center">Read all notifications</a></li>
                </ul>
              </li-->
              <!--li class="dropdown dropdown-notification nav-item"><a href="#" data-toggle="dropdown" class="nav-link nav-link-label"><i class="ficon icon-mail6"></i><span class="tag tag-pill tag-default tag-info tag-default tag-up">8</span></a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                  <li class="dropdown-menu-header">
                    <h6 class="dropdown-header m-0"><span class="grey darken-2">Messages</span><span class="notification-tag tag tag-default tag-info float-xs-right m-0">4 New</span></h6>
                  </li>
                  <li class="list-group scrollable-container"><a href="javascript:void(0)" class="list-group-item">
                      <div class="media">
                        <div class="media-left">
                        <span class="avatar avatar-sm avatar-online rounded-circle">
                        <img src="{{asset ("admin/app-assets/images/portrait/small/avatar-s-1.png") }}" alt="avatar"><i></i></span></div>
                        <div class="media-body">
                          <h6 class="media-heading">Margaret Govan</h6>
                          <p class="notification-text font-small-3 text-muted">I like your portfolio, let's start the project.</p><small>
                            <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Today</time></small>
                        </div>
                      </div></a><a href="javascript:void(0)" class="list-group-item">
                      <div class="media">
                        <div class="media-left">
                        <span class="avatar avatar-sm avatar-busy rounded-circle">
                        <img src="{{asset ("admin/app-assets/images/portrait/small/avatar-s-2.png") }}" alt="avatar"><i></i></span></div>
                        <div class="media-body">
                          <h6 class="media-heading">Bret Lezama</h6>
                          <p class="notification-text font-small-3 text-muted">I have seen your work, there is</p><small>
                            <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Tuesday</time></small>
                        </div>
                      </div></a><a href="javascript:void(0)" class="list-group-item">
                      <div class="media">
                        <div class="media-left"><span class="avatar avatar-sm avatar-online 
                        rounded-circle"><img
                         src="{{asset ("admin/app-assets/images/portrait/small/avatar-s-3.png") }}" alt="avatar"><i></i></span></div>
                        <div class="media-body">
                          <h6 class="media-heading">Carie Berra</h6>
                          <p class="notification-text font-small-3 text-muted">Can we have call in this week ?</p><small>
                            <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Friday</time></small>
                        </div>
                      </div></a><a href="javascript:void(0)" class="list-group-item">
                      <div class="media">
                        <div class="media-left">
                        <span class="avatar avatar-sm avatar-away rounded-circle">
                        <img src="{{asset ("admin/app-assets/images/portrait/small/avatar-s-6.png") }}" alt="avatar"><i></i></span></div>
                        <div class="media-body">
                          <h6 class="media-heading">Eric Alsobrook</h6>
                          <p class="notification-text font-small-3 text-muted">We have project party this saturday night.</p><small>
                            <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">last month</time></small>
                        </div>
                      </div></a></li>
                  <li class="dropdown-menu-footer"><a href="javascript:void(0)" class="dropdown-item text-muted text-xs-center">Read all messages</a></li>
                </ul>
              </li-->
              <li class="dropdown dropdown-user nav-item"><a href="#" data-toggle="dropdown"
               class="dropdown-toggle nav-link dropdown-user-link">
               <span class="avatar avatar-online">
               <img src="{{asset ("admin/app-assets/images/portrait/small/avatar-s-1.png") }}" alt="avatar">
               <i></i></span>
               <span class="user-name">{{Auth::user()->name}} {{Auth::user()->lastname}}</span></a>
               <div class="dropdown-menu dropdown-menu-right">
                <a href="#" class="dropdown-item"><i class="icon-head"></i> Modifier profil</a>
                
                  <div class="dropdown-divider">
                  </div>
                  
                  <a  class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" >
                                            <i class="icon-power3" aria-hidden="true"></i> Se déconnecter
                                            @if(Auth::check())
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                            @else
                                            <form id="logout-form" action="{{ route('login') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                            @endif
                  </a>
                  
                  
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
    <div id="fullscreen-search" class="fullscreen-search">
      <form class="fullscreen-search-form">
        <input type="search" placeholder="Search..." class="fullscreen-search-input">
        <button type="submit" class="fullscreen-search-submit">Search</button>
      </form>
      <div class="fullscreen-search-content">
        <div class="fullscreen-search-options">
          <div class="row">
            <div class="col-sm-12">
              <fieldset>
                <label class="custom-control custom-checkbox display-inline">
                  <input type="checkbox" class="custom-control-input"><span class="custom-control-indicator"></span><span class="custom-control-description m-0">All</span>
                </label>
                <label class="custom-control custom-checkbox display-inline">
                  <input type="checkbox" class="custom-control-input"><span class="custom-control-indicator"></span><span class="custom-control-description m-0">People</span>
                </label>
                <label class="custom-control custom-checkbox display-inline">
                  <input type="checkbox" class="custom-control-input"><span class="custom-control-indicator"></span><span class="custom-control-description m-0">Project</span>
                </label>
                <label class="custom-control custom-checkbox display-inline">
                  <input type="checkbox" class="custom-control-input"><span class="custom-control-indicator"></span><span class="custom-control-description m-0">Task</span>
                </label>
              </fieldset>
            </div>
          </div>
        </div>
        <div class="fullscreen-search-result mt-2">
          <div class="row">
            <div class="col-lg-4">
              <h3>People</h3>
              <div class="media"><a href="#" class="media-left">
              <img src="{{asset ("admin/app-assets/images/portrait/small/avatar-s-2.png") }}" alt="Generic placeholder image" class="media-object rounded-circle"></a>
                <div class="media-body">
                  <h5 class="media-heading"><a href="#">Karmen Dartez</a></h5>
                  <p class="mb-0"><span class="tag tag-pill mr-1 tag-danger">JavaScript</span><span class="tag tag-pill mr-1 tag-primary">HTML</span></p>
                  <p><span class="font-weight-bold">Sr. Developer - </span><a href="mailto:john@example.com">karmen@example.com</a></p>
                </div>
              </div>
              <div class="media"><a href="#" class="media-left">
              <img src="{{asset ("admin/app-assets/images/portrait/small/avatar-s-3.png") }}" alt="Generic placeholder image" class="media-object rounded-circle"></a>
                <div class="media-body">
                  <h5 class="media-heading"><a href="#">Scot Loh</a></h5>
                  <p class="mb-0"><span class="tag tag-pill mr-1 tag-danger">PhotoShop</span><span class="tag tag-pill mr-1 tag-warning">UX</span></p>
                  <p><span class="font-weight-bold">Sr. UI/UX Desugner - </span><a href="mailto:john@example.com">scot@example.com</a></p>
                </div>
              </div>
              <div class="media"><a href="#" class="media-left">
              <img src="{{asset ("admin/app-assets/images/portrait/small/avatar-s-5.png") }}" alt="Generic placeholder image" class="media-object rounded-circle"></a>
                <div class="media-body">
                  <h5 class="media-heading"><a href="#">Kim Willmore</a></h5>
                  <p class="mb-0"><span class="tag tag-pill mr-1 tag-warning">CSS</span><span class="tag tag-pill mr-1 tag-danger">HTML</span></p>
                  <p><span class="font-weight-bold">UI Developer - </span><a href="mailto:john@example.com">kim@example.com</a></p>
                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <h3>Project</h3>
              <div class="media">
                <div class="media-body">
                  <h5 class="media-heading"><a href="#">WordPress Template Support</a></h5>
                  <progress value="25" max="100" class="progress progress-xs progress-success mb-0">25%</progress>
                  <p class="mb-0">Collicitudin vel metus scelerisque ante  commodo.</p>
                  <p><span class="tag tag-pill tag-success">In Progress</span><span class="tag tag-default tag-default float-sm-right">25% Completed</span></p>
                </div>
              </div>
              <div class="media">
                <div class="media-body">
                  <h5 class="media-heading"><a href="#">Application UI/UX</a></h5>
                  <progress value="100" max="100" class="progress progress-xs progress-info mb-0">100%</progress>
                  <p class="mb-0">Cetus scelerisque ante sollicitudin commodo.</p>
                  <p><span class="tag tag-pill tag-info">Completed</span><span class="tag tag-default tag-default float-sm-right">100% Completed</span></p>
                </div>
              </div>
              <div class="media">
                <div class="media-body">
                  <h5 class="media-heading"><a href="#">SEO Project</a></h5>
                  <progress value="65" max="100" class="progress progress-xs progress-warning mb-0">65%</progress>
                  <p class="mb-0">Notifications scelerisque ante sollicitudin commodo.</p>
                  <p><span class="tag tag-pill tag-warning">Delayed</span><span class="tag tag-default tag-default float-sm-right">65% Completed</span></p>
                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <h3>Task</h3>
              <div class="media">
                <div class="media-body">
                  <h5 class="media-heading"><a href="#">Create the new layout for menu</a></h5>
                  <p class="mb-0">Pcelerisque ulla vel metus  ante sollicitudin commodo.</p>
                  <p><span class="tag tag-pill tag-danger">Open</span><span class="tag tag-default tag-default tag-default tag-icon float-sm-right"><i class="icon-calendar5"></i> 22 January, 16</span></p>
                </div>
              </div>
              <div class="media">
                <div class="media-body">
                  <h5 class="media-heading"><a href="#">Addition features on footer</a></h5>
                  <p class="mb-0">Tuaiulla vel metus scelerisque ante sollicitudin commodo.</p>
                  <p><span class="tag tag-pill tag-warning">On hold</span><span class="tag tag-default tag-default tag-default tag-icon float-sm-right"><i class="icon-calendar5"></i> 24 January, 16</span></p>
                </div>
              </div>
              <div class="media">
                <div class="media-body">
                  <h5 class="media-heading"><a href="#">Remove TODO comments</a></h5>
                  <p class="mb-0">Mulullametu vel  scelerisque ante sollicitudin commodo.</p>
                  <p><span class="tag tag-pill tag-info">Resolved</span><span class="tag tag-default tag-default tag-default tag-icon float-sm-right"><i class="icon-calendar5"></i> 25 January, 16</span></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div><span class="fullscreen-search-close"></span>
    </div>
    <div class="fullscreen-search-overlay"></div>

    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
   
        <div class="media px-1">
          <br/>
              <div class="media-left media-middle">

                  <i class="icon-box font-large-1 blue-grey"></i>
              </div>
              <div id="score" class="media-body text-xs-right">
                
                  <span class="font-large-2 text-bold-300 info">{{$points}}</span>
              </div>
              <p class="text-muted">Total Points <span class="info float-xs-right"><i class="icon-arrow-up4 info"></i> 16.89%</span></p>
              <progress class="progress progress-sm progress-info" value="80" max="100"></progress>
          </div>
          <div>
                <br/>

                <div id="proposition">
                
                </div>   
                
          </div>
        
    </div>


    <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><!-- Sales stats -->

     @if (session('ok'))
    <br/>
    <div role="alert" class="alert alert-success alert-dismissible">
      <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><span class="icon mdi mdi-check"></span>
      <strong></strong> {{ session('ok') }}
    </div>
   @endif
   @if (session('danger'))
<br/>
<div role="alert" class="alert alert-danger alert-dismissible">
    <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><span class="icon mdi mdi-check"></span>
    <strong>Erreur!</strong> {{ session('danger') }}
</div>
@endif


<!-- Sales by Campaigns & Year -->
<div class="row">
    <div class="col-xl-8 col-lg-12">
        <div class="card">
            <div class="card-header no-border-bottom">
                <h4 class="card-title pb-1">Votre jeu</h4>


                <div class="media-body text-xs">
                  <span class="font-large-1 text-bold-200 info">
                    @for($i=0;$i<strlen($wordhardshuffle);$i++)
                            {{strtoupper($wordhardshuffle[$i])}}
                @endfor

               
              </span>
              </div>

                
                <form method="POST" id="formulaire" action="{{ url('/hard') }}" aria-label="{{ __('Login') }}">
                
                @csrf  

                <input type="hidden" name="wordhardshuffle" id="wordshuffle" value="{{$wordhardshuffle}}">                

                    <div class="form-group for-control">
                   
                   </div>

                    <fieldset class="form-group position-relative has-icon-left">
                        <input type="text" class="form-control form-control-lg input-lg {{ $errors->has('answer') ? ' is-invalid' : '' }}"
                         name="answer" id="anwser" placeholder="Saisissez votre proposition" required>
                        <div class="form-control-position">
                          
                        </div>

                        @if ($errors->has('answer'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('answer') }}</strong>
                                </span>
                            @endif
                    </fieldset>

                    
                    <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="icon-location4"></i> Tenter</button>
                </form>

                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a id="reload"><i class="icon-reload"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div id="sales-campaigns" class="height-300 echart-container"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-12">
        <div class="card card-inverse bg-danger">

        <div id="countup">
 
  
  <span id="days" style="visibility:hidden" class="badge badge-success ">00 </span>
  
 
  <h1 style="color:white; font-weight:bold;">
  <span id="hours" class="badge badge-primary ">00 </span> :
  <span id="minutes" class="badge badge-primary ">00 </span> :
  <span id="seconds" class="badge badge-primary ">00 </span></h1>


</div>
            
        </div>
    </div>
</div>


        </div>
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <footer class="footer footer-static footer-light navbar-border">
      <p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; 2017 <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank" class="text-bold-800 grey darken-2">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted & Made with <i class="icon-heart5 pink"></i></span></p>
    </footer>

    <!-- BEGIN VENDOR JS-->
    <script src="{{asset ("admin/app-assets/js/core/libraries/jquery.min.js") }}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/ui/tether.min.js") }}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/js/core/libraries/bootstrap.min.js") }}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js") }}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/ui/unison.min.js") }}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/ui/blockUI.min.js") }}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/ui/jquery.matchHeight-min.js") }}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/ui/jquery-sliding-menu.js") }}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/sliders/slick/slick.min.js") }}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/ui/screenfull.min.js") }}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/extensions/pace.min.js") }}" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="{{asset ("admin/app-assets/vendors/js/charts/raphael-min.js") }}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/charts/morris.min.js") }}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/charts/chart.min.js") }}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js") }}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js") }}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/extensions/moment.min.js") }}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/extensions/underscore-min.js") }}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/extensions/clndr.min.js") }}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/charts/echarts/echarts.js") }}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/extensions/unslider-min.js") }}" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN ROBUST JS-->
    <script src="{{asset ("admin/app-assets/js/core/app-menu.js") }}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/js/core/app.js") }}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/js/scripts/ui/fullscreenSearch.js") }}" type="text/javascript"></script>
    <!-- END ROBUST JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{asset ("admin/app-assets/js/scripts/pages/dashboard-ecommerce.js") }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->



<script type="text/javascript">
    
    /*
* Basic Count Up from Date and Time
* Author: @guwii / guwii.com
*/
window.onload=function() {
  // Month,Day,Year,Hour,Minute,Second

  upTime(Date.now()); // ****** Change this line!
}
function upTime(countTo) {
  now = new Date();
  countTo = new Date(countTo);
  difference = (now-countTo);

  days=Math.floor(difference/(60*60*1000*24)*1);
  hours=Math.floor((difference%(60*60*1000*24))/(60*60*1000)*1);
  mins=Math.floor(((difference%(60*60*1000*24))%(60*60*1000))/(60*1000)*1);
  secs=Math.floor((((difference%(60*60*1000*24))%(60*60*1000))%(60*1000))/1000*1);

  document.getElementById('days').firstChild.nodeValue = days;
  document.getElementById('hours').firstChild.nodeValue = hours;
  document.getElementById('minutes').firstChild.nodeValue = mins;
  document.getElementById('seconds').firstChild.nodeValue = secs;

  clearTimeout(upTime.to);
  upTime.to=setTimeout(function(){ upTime(countTo); },1000);
}
</script>

<script>

$(document).on('submit', '#formulaire', function(e){

e.preventDefault();

$.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name =_token]').attr('content') }
});

$.ajax({
    url: "{{ url('/score')}}",
    type: 'post',
    data: {'reponse':$('#answer').val(),
          'scores':$('#scores').val(),
          'wordshuffle':$('#wordshuffle').val()
          },
    success: function(data){
        //console.log('gbodome :' +data.score);
       
      $('#score').html(data.score);
      //$('#inc').html(data.score);
    },
    error: function(err){
      console.log('error :'+JSON.stringify(err));
    }
  });
});

</script>


<script type="text/javascript">

$(document).on('submit', '#formulaire', function(e){

e.preventDefault();

$.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name =_token]').attr('content') }
});

$.ajax({
    url: "{{ url('hard')}}",
    type: 'post',
    data: {'reponse':$('#answer').val(),
      'wordshuffle':$('#wordshuffle').val()
          },
    success: function(data){
        console.log('Madaliou :' + data.msg);
        console.log('vvv :'+$('#wordshuffle').val());
      $('#proposition').append(data.msg);
      //$('#inc').html(data.score);
    },
    error: function(err){
      console.log('erreur :'+ JSON.stringify(err));
    }
  });
});

</script>


<script type="text/javascript">

$('#reload').on('click', function(e){

  e.preventDefault();

  $.ajaxSetup({
    headers: { 'X-CSRF-Token' : $('meta[name =_token]').attr('content') }
  });

  $.ajax({
      url: "{{ url('shuffle')}}",
      type: 'post',
      data: {'choice': 'hard' },
      success: function(data){
          //console.log('Retour :' +data)
          //console.log(data.tab);
        //$('#proposition').append(data.msg);
        $('#shuffle').html(data.msg);
        $('#wordshuffle').val(data.wordshuffle);
        $('#proposition').html(data.propo);
      },
      error: function(err){
        console.log('erreur shuffling :'+ JSON.stringify(err));
      }
    });
});

</script>



  </body>
</html>
