@extends('layouts.manager')
@section('title','CIGC | Admin')
@section('content')

<!-- DASHBOARD CONTENT -->

  

<div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
		@if (session('ok'))
    <br/>
    <div role="alert" class="alert alert-success alert-dismissible">
      <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><span class="icon mdi mdi-check"></span>
      <strong></strong> {{ session('ok') }}
    </div>
   @endif
   @if (session('danger'))
<br/>
<div role="alert" class="alert alert-danger alert-dismissible">
    <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><span class="icon mdi mdi-check"></span>
    <strong>Erreur!</strong> {{ session('danger') }}
</div>
@endif

<div class="page-header-actions">
		<!-- Button trigger modal -->

	
									</div>

					<div class="col-lg-4 col-md-6 col-sm-12">
								<div class="form-group">						

									<!-- Modal -->
									<div class="modal fade text-xs-left" id="iconForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" aria-hidden="true">
									  <div class="modal-dialog" role="document">
										<div class="modal-content">
										  <div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											  <span aria-hidden="true">&times;</span>
											</button>
											<h3 class="modal-title" id="myModalLabel34">Ajout Utilisateur</h3>
										  </div>
										  <form action="{{url('user')}}" method= "POST">
										 @csrf
											<div class="modal-body">
											<label>Nom: </label>
												<div class="form-group position-relative has-icon-left">
													<input type="text" placeholder="Nom" name="name" class="form-control" required>
													<div class="form-control-position">
														<i class="icon-user4 text-muted"></i>
													</div>

													
												</div>
												<label>Prénom: </label>
												<div class="form-group position-relative has-icon-left">
													<input type="text" placeholder="Prénom" name="lastname" class="form-control" required>
													<div class="form-control-position">
														<i class="icon-user4 text-muted"></i>
													</div>
												</div>
												<label>Email: </label>
												<div class="form-group position-relative has-icon-left">
													<input type="text" placeholder="Email" name="email" class="form-control" required>
													<div class="form-control-position">
														<i class="icon-mail6 text-muted"></i>
													</div>
													@if ($errors->has('email'))
													<span class="help-block">
														<strong style="color:rgb(255,0,40);">{{ $errors->first('email') }}</strong>
													</span>
													@endif
												</div>

												<label>Mot de passe: </label>
												<div class="form-group position-relative has-icon-left">
													<input type="password" name="password" placeholder="Mot de passe" class="form-control" required>
													<div class="form-control-position">
														<i class="icon-padlock1 text-muted"></i>
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Annuler">
												<input type="submit" class="btn btn-outline-primary btn-lg" value="Ok">
											</div>
										  </form>
										</div>
									  </div>
									</div>
								</div>
							</div>


<div class="card-body collapse in">
	<div class="card-block card-dashboard">
        <table class="table table-striped table-bordered dataex-html5-export">
							<thead>
								<tr>
								<th>#</th>
								<th>Auteur</th>
									<th>Niveau</th>
									<th>Score</th>
									<th>Date</th>
															
								</tr>
							</thead>
							<tbody>
							<?php $i=0;?>
@foreach ($games as $game)
  <tr>
	<td> <?php echo $i = $i + 1;?></td>
	<td>
	@if($game->user_id == Auth::user()->id)
	Vous
	@else
	{{$game->user->name}} {{$game->user->lastname}}  
	
	@endif                       
	</td>
	<td>
	@if($game->level == "easy")
	Facile
	@elseif($game->level == "normal")
	Normal
	@elseif($game->level == "hard")
	Difficile
	@endif
	</td>
	<td>{{$game->points}}</td>
	<td>{!! date('d-m-Y à H:i:s', strtotime($game->created_at)) !!}</td>

  </tr>
@endforeach
							

							</tbody>
							<tfoot>
								<tr>
								<th>#</th>
								<th>Auteur</th>
								<th>Niveau</th>
									<th>Score</th>
									<th>Date</th>
										
								</tr>
							</tfoot>
						</table>

	</div>
</div>

<!-- Sales by Campaigns & Year >
<div class="row">
    <div class="col-xl-8 col-lg-12">
        <div class="card">
            <div class="card-header no-border-bottom">
                <h4 class="card-title pb-1">Sales by Campaigns</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div id="sales-campaigns" class="height-300 echart-container"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-12">
        <div class="card card-inverse bg-danger">
            <div class="card-header no-border-bottom">
                <h4 class="card-title">Yearly Sales</h4>
            </div>
            <div class="card-body">
                <div class="chartjs">
                    <canvas id="yearly-sales" class="height-350 px-2 pt-2"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ Sales by Campaigns & Year -->

<!-- Top Selling Phones & Customer Browser's Stats >
<div class="row">
    <div class="col-md-6 col-sm-12">
        <div class="card card-inverse bg-gradient-y-danger">
            <div class="card-header no-border-bottom">
                <h4 class="card-title">Customer Browser's Stats</h4>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div id="browser-stats" class="height-300 echart-container"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-sm-12">
        <div class="card card-inverse bg-success">
            <div class="card-header no-border-bottom">
                <h4 class="card-title">Top Selling Phones</h4>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div id="top-selling-phones-doughnut" class="height-300 echart-container"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ Top Selling Phones & Customer Browser's Stats -->

    </div>
</div>

				<!--div class="row">
                    <div class="col-md-4 col-sm-12">
                        <h5>Custom Icon</h5>
                        <p><code>imageUrl</code> is used to add a customized icon for the modal. Should contain a string with the path to the image.</p>
                        <button type="button" class="btn btn-lg btn-block btn-outline-primary mb-2" id="custom-icon">Custom Icon</button>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <h5>Auto Close Timer</h5>
                        <p>A message with auto close timer. <code>timer</code> is default set to <code>null</code>. You can set timer in ms (milliseconds).</p>
                        <button type="button" class="btn btn-lg btn-block btn-outline-primary mb-2" id="auto-close">Auto Close</button>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <h5>Allow Outside Click</h5>
                        <p>If <code>allowOutsideClick</code> is set to <code>true</code>, the user can dismiss the modal by clicking outside it.</p>
                        <button type="button" class="btn btn-lg btn-block btn-outline-primary mb-2" id="outside-click">Click Outside</button>
                    </div>
                </div-->

				<!--div class="card-block">
                <div class="row mb-3">
                    <div class="col-md-4 col-sm-12">
                        <button type="button" class="btn btn-lg btn-block btn-outline-primary mb-2" id="basic-alert">Basic</button>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <button type="button" class="btn btn-lg btn-block btn-outline-primary mb-2" id="with-title">With Title</button>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <button type="button" class="btn btn-lg btn-block btn-outline-primary mb-2" id="html-alert">HTML</button>
                    </div>
                </div>
            </div-->
    </div>

@stop
