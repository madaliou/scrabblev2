@extends('layouts.manager')
@section('title','CIGC | Admin')
@section('content')



<div class="app-content content container-fluid">
    <div class="content-wrapper">
      <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Modification Utilisateur</h2>
          </div>
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('user')}}">Accueil</a>
                </li>
                <li class="breadcrumb-item"><a href="{{url('user')}}">Utilisateurs</a>
                </li>
                <li class="breadcrumb-item active">Modifier
                </li>
              </ol>
            </div>
      </div>
    </div>
    <div class="content-body">
<!-- Page -->



    <div class="page-content">
      <!-- Panel Form Elements -->
      
        
        
           <div class="col-lg-6">

           {!! Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal ']) !!}
               

                  <div class="form-group">
                  
                        <label class="col-md-2 control-label">Nom</label>
                        
                        <div class="col-md-10">

                          {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'saisir le nom']) !!}


                        {!! $errors->first('name', '<small class="help-block">:message</small>') !!}
                        <br/>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Prénom</label>
                        <div class="col-md-10">

                        {!! Form::text('lastname', null, ['class' => 'form-control', 'placeholder' => 'saisir le nom']) !!}

                       

                        {!! $errors->first('lastname', '<small class="help-block">:message</small>') !!}
                        <br/>
                        </div>

                    </div>

                     <div class="form-group">
                        <label class="col-md-2 control-label">Login</label>
                        <div class="col-md-10">

                        {!! Form::text('login', null, ['class' => 'form-control', 'placeholder' => 'saisir le login']) !!}

                       

                        {!! $errors->first('login', '<small class="help-block">:message</small>') !!}
                        <br/>
                        </div>

                    </div>



                     <div class="form-group">
                        <label class="col-md-2 control-label">Email</label>
                        <div class="col-md-10">
                        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'saisir le nom']) !!}

                        {!! $errors->first('email', '<small class="help-block">:message</small>') !!}
                        <br/>
                        </div>
                    </div>                     
                    <div class="form-group">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6">
                    <br/> <br/>
                    {!! Form::submit('Valider', ['class' => 'btn btn-primary ', 'style' => 'width : 90px;']) !!}
                    <br/> <br/>  <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/>
                    </div>
                  

                       
                 

                  </div>
                </form>
            </div><!-- end col -->

        </div>
      </div>
    
  </div>

 
    </div>
  </div>

@endsection
