@extends('layouts.manager')
@section('title','CIGC | Admin')
@section('content')

<!-- DASHBOARD CONTENT -->

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
    	</div>
        <div class="content-body">
				@if (session('ok'))
			<br/>
			<div role="alert" class="alert alert-success alert-dismissible">
			<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><span class="icon mdi mdi-check"></span>
			<strong></strong> {{ session('ok') }}
			</div>
		@endif
		@if (session('danger'))
		<br/>
		<div role="alert" class="alert alert-danger alert-dismissible">
			<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><span class="icon mdi mdi-check"></span>
			<strong>Erreur!</strong> {{ session('danger') }}
		</div>
		@endif

		<div class="page-header-actions">
			<!-- Button trigger modal -->

			@if(Auth::user()->role == "admin")
			<button type="button" id="ajoutModal" class="btn btn-success mrg-b-lg pull-left" data-toggle="modal" data-target="#iconForm">
			Ajouter
			</button>
			@endif
		</div>

		<div class="col-lg-4 col-md-6 col-sm-12">
			<div class="form-group">						
					<!-- Modal -->
					<div class="modal fade text-xs-left" id="iconForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
								</button>
								<h3 class="modal-title" id="myModalLabel34">Ajout Utilisateur</h3>
							</div>
							<form action="{{url('user')}}" method= "POST">
							@csrf
								<div class="modal-body">
								<label>Nom: </label>
									<div class="form-group position-relative has-icon-left">
										<input type="text" placeholder="Nom" name="name" class="form-control" required>
										<div class="form-control-position">
											<i class="icon-user4 text-muted"></i>
										</div>

										
									</div>
									<label>Prénom: </label>
									<div class="form-group position-relative has-icon-left">
										<input type="text" placeholder="Prénom" name="lastname" class="form-control" required>
										<div class="form-control-position">
											<i class="icon-user4 text-muted"></i>
										</div>
									</div>
									<label>Login: </label>
									<div class="form-group position-relative has-icon-left">
										<input type="text" placeholder="Login" name="login" class="form-control" required>
										<div class="form-control-position">
											<i class="icon-mail6 text-muted"></i>
										</div>
										@if ($errors->has('login'))
										<span class="help-block">
											<strong style="color:rgb(255,0,40);">{{ $errors->first('login') }}</strong>
										</span>
										@endif
									</div>
									<label>Email: </label>
									<div class="form-group position-relative has-icon-left">
										<input type="text" placeholder="Email" name="email" class="form-control" required>
										<div class="form-control-position">
											<i class="icon-mail6 text-muted"></i>
										</div>
										@if ($errors->has('email'))
										<span class="help-block">
											<strong style="color:rgb(255,0,40);">{{ $errors->first('email') }}</strong>
										</span>
										@endif
									</div>

									<label>Mot de passe: </label>
									<div class="form-group position-relative has-icon-left">
										<input type="password" name="password" placeholder="Mot de passe" class="form-control" required>
										<div class="form-control-position">
											<i class="icon-padlock1 text-muted"></i>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Annuler">
									<input type="submit" class="btn btn-outline-primary btn-lg" value="Ok">
								</div>
							</form>
							</div>
						</div>
					</div>
			</div>
		</div>

		<div class="panel">

		<h1>Liste des utilisateurs</h1> 

			<div class="card-body collapse in">
				<div class="card-block card-dashboard">

				

					<table class="table table-striped table-bordered dataex-html5-export">
						<thead>
							<tr>
							<th>#</th>
								<th>Nom</th>
								<th>Prénom</th>
								<th>Login</th>
								<th>Email</th>
							
								<th>Modifier</th>
								<th>Supprimer</th>							
							</tr>
						</thead>
						<tbody>
							<?php $i=0;?>
							@foreach ($users as $user)
							<tr>
							<td> <?php echo $i = $i + 1;?></td>
							<td>{{$user->name}}</td>
							<td>{{$user->lastname}}</td>
							<td>{{$user->login}}</td>
							<td>{{$user->email}}</td>

							<td><a href="{{action('UserController@edit', $user['id'])}}" class="btn btn-success">
							<i class="icon-pencil3"></i>
							</a>
												
							</td>

							<td>
							<form action="{{action('UserController@destroy', $user['id'])}}" method="post">
									@csrf
									<input name="_method" type="hidden" value="DELETE">
									<button class="btn btn-danger" type="submit"
									onclick="return confirm('Voulez-vous vraiment supprimer ?');"><i class="icon-trash-o"></i></button>
								</form>
							</td>

							</tr>
							@endforeach								
						</tbody>
						<tfoot>
							<tr>
							<th>#</th>
							<th>Nom</th>
								<th>Prénom</th>
								<th>Login</th>
								<th>Email</th>
								<th>Modifier</th>
								<th>Supprimer</th>
							</tr>
						</tfoot>
					</table>

				</div>
				</div>

			</div>


		</div>
	</div>					
</div>


@stop

