@extends('layouts.manager')
@section('title','MooziCash | Administration-Utilisateur-Detail')
@section('contenu')

<!-- Page -->

    <div class="page-content page animsition container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">


              <div class="col-md-12 col-xs-12 masonry-item" style="background: linear-gradient(#463f96,#009cd7);">
                <div class="widget" >
                  <div class="widget-header white padding-30 clearfix" style="background: linear-gradient(#463f96,#009cd7);">
                    <a class="avatar avatar-100 pull-left margin-right-20" href="javascript:void(0)">
                    <img src="{{asset ("admin/global/portraits/user.png")}}" alt="...">
                    </a>
                    <div class="pull-left">
                      <div class="font-size-20 margin-bottom-15">
                      @if($user->role == "adherent")
                      <p>  {{$user->corporate->name}}  </p>
                       @elseif($user->role == "particulier")
                      <p>{{$user->particular->name}} {{$user->particular->lastname}}  </p>
                      @elseif($user->role == "moderateur")
                      Modérateur
                      @elseif($user->role == "admin")
                      Admin
                      @elseif($user->role == "caisse")
                      Caisse de {{$user->corporate->name}}
                      @endif
                       </div>
                      <p class="margin-bottom-5 text-nowrap"><i class="icon wb-map margin-right-10" aria-hidden="true"></i><span class="text-break">
                        @if($user->role == "adherent")
                         {{$user->corporate->address}}, Togo
                       @elseif($user->role == "particulier")
                         {{$user->particular->district->name}}, Togo
                      @else
                      Non renseigné
                       @endif </span>
                      </p>
                      <p class="margin-bottom-5 text-nowrap"><i class="icon wb-envelope margin-right-10" aria-hidden="true"></i>
                        <span
                        class="text-break">{{$user->email}}</span>
                      </p>
                      <p class="margin-bottom-5 text-nowrap"><i class="icon fa-phone margin-right-10" aria-hidden="true"></i>
                        <span
                        class="text-break">+{{$user->phone}}</span>
                      </p>
                    </div>
                  </div>
                  <div class="widget-content">
                    <div class="row no-space padding-vertical-20 padding-horizontal-30 text-center">
                      <div class="col-xs-4">
                        <div class="counter">
                          <div class="counter-label">Parrain</div>
                          <span class="counter-number cyan-600">
                            @if($user->role == "adherent")
                                @if($user->corporate->godfather)
                                  @if($user->corporate->godfather->role == "adherent")
                                      <p>  {{$user->corporate->godfather->corporate->name}}  </p>
                                  @elseif($user->corporate->godfather->role == "particulier")
                                    <p>{{$user->corporate->godfather->particular->name}} {{$user->corporate->godfather->particular->lastname}}  </p>
                                  @endif
                                @else
                                Aucun
                                @endif
                            @elseif($user->role == "particulier")
                                  @if($user->particular->godfather)
                                    @if($user->particular->godfather->role == "adherent")
                                        <p>  {{$user->particular->godfather->corporate->name}}  </p>
                                    @elseif($user->particular->godfather->role == "particulier")
                                        <p>{{$user->particular->godfather->particular->name}} {{$user->particular->godfather->particular->lastname}}  </p>

                                    @endif
                                  @else
                                    Aucun
                                  @endif
                            @else
                            Aucun
                            @endif

                           </span>

                        </div>
                        <br/><br/>
                      </div>

                      <div class="col-xs-4">
                        <div class="counter">

                          <div class="counter-label">Filleuls</div>
                            <span class="counter-number cyan-600">{{sizeof($tableau)}}</span>
                        </div>
                      </div>

                      <div class="col-xs-4">
                        <div class="counter">
                          <div class="counter-label">Moozi</div>
                          <span class="counter-number cyan-600">
                             @if($user->role == "adherent" OR $user->role == "particulier" )
                             {{$account->moozistotal}}
                             @else
                             0
                             @endif

                            </span>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              </div>
          <!-- end col -->

        </div>
      </div>
    </div>


@endsection
