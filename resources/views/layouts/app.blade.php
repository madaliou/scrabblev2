<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>@yield('title')</title>
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset ("admin/app-assets/images/ico/apple-icon-60.png") }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset ("admin/app-assets/images/ico/apple-icon-76.png") }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset ("admin/app-assets/images/ico/apple-icon-120.png") }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset ("admin/app-assets/images/ico/apple-icon-152.png") }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset ("admin/app-assets/images/ico/favicon.ico") }}">
    <link rel="shortcut icon" type="image/png" href="{{asset ("admin/app-assets/images/ico/favicon-32.png") }}">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/bootstrap.css") }}">
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/fonts/icomoon.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/fonts/flag-icon-css/css/flag-icon.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/vendors/css/sliders/slick/slick.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/vendors/css/extensions/pace.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/vendors/css/charts/jquery-jvectormap-2.0.3.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/vendors/css/charts/morris.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/vendors/css/extensions/unslider.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/vendors/css/weather-icons/climacons.min.css") }}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN ROBUST CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/bootstrap-extended.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/app.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/colors.css") }}">
    <!-- END ROBUST CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/core/menu/menu-types/vertical-menu.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/core/colors/palette-gradient.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/plugins/calendars/clndr.css") }}">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/assets/css/style.css") }}">
    <!-- END Custom CSS-->

    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css") }}">
  </head>
  <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

    @include('includes.header')



    @yield('content')

    @include('includes.footer')



<!-- BEGIN VENDOR JS-->
    <script src="{{asset ("admin/app-assets/js/core/libraries/jquery.min.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/ui/tether.min.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/js/core/libraries/bootstrap.min.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/ui/unison.min.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/ui/blockUI.min.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/ui/jquery.matchHeight-min.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/ui/jquery-sliding-menu.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/sliders/slick/slick.min.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/ui/screenfull.min.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/extensions/pace.min.js")}}" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="{{asset ("admin/app-assets/vendors/js/charts/raphael-min.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/charts/morris.min.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/charts/chart.min.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/extensions/moment.min.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/extensions/underscore-min.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/extensions/clndr.min.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/charts/echarts/echarts.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/extensions/unslider-min.js")}}" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN ROBUST JS-->
    <script src="{{asset ("admin/app-assets/js/core/app-menu.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/js/core/app.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/js/scripts/ui/fullscreenSearch.js")}}" type="text/javascript"></script>
    <!-- END ROBUST JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{asset ("admin/app-assets/js/scripts/pages/dashboard-ecommerce.js")}}" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->

    <script src="{{asset ("admin/app-assets/vendors/js/tables/jquery.dataTables.min.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js")}}" type="text/javascript"></script>
    <script src="{{asset ("admin/app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js")}}" type="text/javascript"></script>
  </body>
</html>