<!-- main menu-->
    <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
      <!-- main menu header-->
      <!-- / main menu header-->
      
      <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
          @if(Auth::user()->role == "admin")
          <li class=" nav-item"><a href="javascript: void(0);"><i class="icon-user1"></i><span data-i18n="nav.dash.main"
           class="menu-title">Utilisateurs</span>
           <span class="tag tag tag-primary tag-pill float-xs-right mr-2">
           
           {{count(App\User :: where('role','!=', 'admin')->get())}}
           
            </span></a>
            <ul class="menu-content">
              <li class="javascript: void(0);"><a href="{{url('user')}}" data-i18n="nav.dash.ecommerce" class="menu-item">Liste</a>
              </li>
            </ul>
          </li>
          @endif

          <li class=" nav-item"><a href="javascript: void(0);"><i class="icon-book"></i><span data-i18n="nav.dash.main"
            class="menu-title">Témoignages</span>
            <span class="tag tag tag-primary tag-pill float-xs-right mr-2">            
                {{count(App\Testimonial ::all())}}
            </span></a>
              <ul class="menu-content">
                <li class="javascript: void(0);"><a href="{{url('testimonial')}}" data-i18n="nav.dash.ecommerce" class="menu-item">Liste</a>
                </li>
              </ul>
           </li>

        

           <li class=" nav-item"><a href="javascript: void(0);"><i class="icon-network"></i><span data-i18n="nav.dash.main"
            class="menu-title">Réseau</span>
            <span class="tag tag tag-primary tag-pill float-xs-right mr-2">            
                {{count(App\Network :: all())}}  
            </span></a>
              <ul class="menu-content">
                <li class="javascript: void(0);"><a href="{{url('network')}}" data-i18n="nav.dash.ecommerce" class="menu-item">Liste</a>
                </li>
              </ul>
           </li>

           <li class=" nav-item"><a href=""><i class="icon-home3"></i><span data-i18n="nav.dash.main"
            class="menu-title">Partenaire</span>
            <span class="tag tag tag-primary tag-pill float-xs-right mr-2">            
                {{count(App\Partner :: all())}}  
            </span></a>
              <ul class="menu-content">
                <li class=""><a href="{{url('partner')}}" data-i18n="nav.dash.ecommerce" class="menu-item">Liste</a>
                </li>
              </ul>
           </li>

           <li class=" nav-item"><a href="{{url('price')}}"><i class="icon-dollar"></i><span data-i18n="nav.dash.main"
            class="menu-title">Prix</span>
            </a>
              
           </li>
           ----------------------------------------------------
                                <p style="text-align:center;">DIASPORA</p>  

           <li class=" nav-item"><a href="javascript: void(0);"><i class="icon-network"></i><span data-i18n="nav.dash.main"
            class="menu-title">Témoignages</span>
            <span class="tag tag tag-primary tag-pill float-xs-right mr-2">            
                {{count(App\Diaspotestimonial :: all())}}  
            </span></a>
              <ul class="menu-content">
                <li class="javascript: void(0);"><a href="{{url('diaspotestimonial')}}" data-i18n="nav.dash.ecommerce" class="menu-item">Liste</a>
                </li>
              </ul>
           </li>



           <li class=" nav-item"><a href="{{url('diasporaprice')}}"><i class="icon-dollar"></i><span data-i18n="nav.dash.main"
            class="menu-title">Prix de la diaspora</span>
            </a>
              
           </li>


          
           
             
            </ul>
          </li>

          </ul>
           </div>
         
      <!-- /main menu content-->

<!-- main menu footer-->
<div class="main-menu-footer footer-close">
        <div class="header text-xs-center"><a href="#" class="col-xs-12 footer-toggle"><i class="icon-ios-arrow-up"></i></a></div>
        <div class="content">
          <div class="insights">
            <div class="col-xs-12">
              <p>Product Delivery</p>
              <progress value="25" max="100" class="progress progress-xs progress-success">25%</progress>
            </div>
            <div class="col-xs-12">
              <p>Targeted Sales</p>
              <progress value="70" max="100" class="progress progress-xs progress-info">70%</progress>
            </div>
          </div>
          <div class="actions"><a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Settings"><span aria-hidden="true" class="icon-cog3"></span></a><a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Lock"><span aria-hidden="true" class="icon-lock4"></span></a><a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Logout"><span aria-hidden="true" class="icon-power3"></span></a></div>
        </div>
      </div>
      <!-- main menu footer-->
         </div>
    <!-- / main menu-->