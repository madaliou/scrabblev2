 <footer class="footer footer-static footer-light navbar-border">
      <p class="clearfix text-muted text-sm-center mb-0 px-2">
      <span class="float-md-left d-xs-block d-md-inline-block">
      Copyright  &copy; 2019 <a href="https://moozistudio.com" target="_blank" class="text-bold-800 grey darken-2">Moozistudio </a>, Tous droits reservés. </span><span class="float-md-right d-xs-block d-md-inline-block"></span></p>
</footer>