<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="CIGC">
    <meta name="keywords" content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Login - TRANSVIE</title>

    <link rel="shortcut icon" href="{{asset ("admin/app-assets/images/ico/favicon-32x32.png") }}" type="image/x-icon" />

    <link rel="apple-touch-icon" sizes="60x60" href="{{asset ("admin/app-assets/images/ico/apple-icon-60.png") }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset ("admin/app-assets/images/ico/apple-icon-76.png") }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset ("admin/app-assets/images/ico/apple-icon-120.png") }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset ("admin/app-assets/images/ico/apple-icon-152.png") }}">
    <!--link rel="shortcut icon" type="image/x-icon" href="{{asset ("admin/app-assets/images/ico/favicon.ico") }}">
    <link rel="shortcut icon" type="image/png" href="{{asset ("admin/app-assets/images/ico/favicon-32.png") }}"-->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/bootstrap.css") }}">
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/fonts/icomoon.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/fonts/flag-icon-css/css/flag-icon.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/vendors/css/sliders/slick/slick.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/vendors/css/extensions/pace.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/vendors/css/forms/icheck/icheck.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/vendors/css/forms/icheck/custom.css") }}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN ROBUST CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/bootstrap-extended.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/app.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/colors.css") }}">
    <!-- END ROBUST CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/core/menu/menu-types/vertical-menu.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css") }}">
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/app-assets/css/pages/login-register.css") }}">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset ("admin/assets/css/style.css") }}">
    <!-- END Custom CSS-->
  </head>
  <body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column  blank-page blank-page">
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><section class="flexbox-container">
    <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1  box-shadow-2 p-0">
        <div class="card border-grey border-lighten-3 m-0">
            <div class="card-header no-border">
                <div class="card-title text-xs-center">
                @if (session('ok'))
    <br/>
    <div role="alert" class="alert alert-success alert-dismissible">
      <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><span class="icon mdi mdi-check"></span>
      <strong></strong> {{ session('ok') }}
    </div>
   @endif
   @if (session('danger'))
<br/>
<div role="alert" class="alert alert-danger alert-dismissible">
    <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><span class="icon mdi mdi-check"></span>
    <strong>Erreur!</strong> {{ session('danger') }}
</div>
@endif

                    <div class="p-1">
                        <!--img src="{{asset ("admin/app-assets/images/logo/robust-logo-dark.png") }}"-->
                            
                          <span class="font-large-2 text-bold-300 success">TransVie</span>   
                        </div>
                </div>
                <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2"><span>Connectez-vous </span></h6>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">

                <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                
                    @csrf                  


                        <fieldset class="form-group position-relative has-icon-left mb-0">
                            <input type="text" class="form-control form-control-lg input-lg {{ $errors->has('email') ? ' is-invalid' : '' }}" id="user-name"
                            name="email" value="{{ old('email') }}"placeholder="Votre identifiant" required autofocus >
                            
                           
                            <div class="form-control-position">
                                <i class="icon-head"></i>
                            </div>

                             @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong style="color:rgb(255,0,40);">{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </fieldset>


                        <fieldset class="form-group position-relative has-icon-left">
                            <input type="password" class="form-control form-control-lg input-lg {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="user-password" placeholder="Enter Password" required>
                            <div class="form-control-position">
                                <i class="icon-key3"></i>
                            </div>

                            @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong style="color:rgb(255,0,40);">{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </fieldset>

                        <fieldset class="form-group row">
                            <div class="col-md-6 col-xs-12 text-xs-center text-md-left">
                                <!--fieldset>
                                    <input type="checkbox" id="remember-me" class="chk-remember">
                                    <label for="remember-me"> Remember Me</label>
                                </fieldset-->
                            </div>
                            <!--div class="col-md-6 col-xs-12 text-xs-center text-md"><a href="{{ route('password.request') }}" class="card-link">Mot de passe oublié? </a></div-->
                        </fieldset>
                        <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="icon-unlock2"></i> Connexion</button>
                    </form>
                </div>
            </div>
            <div class="card-footer">
                <!--div class="">
                    <p class="float-sm-left text-xs-center m-0"><a href="recover-password.html" class="card-link">Recover password</a></p>
                    <p class="float-sm-right text-xs-center m-0">New to Robust? <a href="register-simple.html" class="card-link">Sign Up</a></p>
                </div-->
            </div>
        </div>
    </div>
</section>

        </div>
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->

    <!-- BEGIN VENDOR JS-->
    <script src="{{asset ("admin/app-assets/js/core/libraries/jquery.min.js") }}"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/ui/tether.min.js") }}"></script>
    <script src="{{asset ("admin/app-assets/js/core/libraries/bootstrap.min.js") }}"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js") }}"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/ui/unison.min.js") }}"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/ui/blockUI.min.js") }}"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/ui/jquery.matchHeight-min.js") }}"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/ui/jquery-sliding-menu.js") }}"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/sliders/slick/slick.min.js") }}"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/ui/screenfull.min.js") }}"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/extensions/pace.min.js") }}"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="{{asset ("admin/app-assets/vendors/js/forms/icheck/icheck.min.js") }}"></script>
    <script src="{{asset ("admin/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js") }}"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN ROBUST JS-->
    <script src="{{asset ("admin/app-assets/js/core/app-menu.js") }}"></script>
    <script src="{{asset ("admin/app-assets/js/core/app.js") }}"></script>
    <script src="{{asset ("admin/app-assets/js/scripts/ui/fullscreenSearch.js") }}"></script>
    <!-- END ROBUST JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{asset ("admin/app-assets/js/scripts/forms/form-login-register.js") }}"></script>
    <!-- END PAGE LEVEL JS-->
  </body>
</html>
