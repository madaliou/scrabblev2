<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('login')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('role');

            $table->integer('parameter_id')->default(1)->unsigned();
            $table->foreign('parameter_id')->references('id')->on('parameters')->onDelete('restrict');
            $table->rememberToken();
            $table->timestamps();
        });
        \App\User::create([ 'firstname'=>'admin','lastname'=>'CIGC', 'login'=>'admin', 'email'=>'admin@admin.com','role'=>'admin','password'=>\Illuminate\Support\Facades\Hash::make('admin')]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table) {
			$table->dropForeign('users_parameter_id_foreign');
        });
        
        Schema::dropIfExists('users');
    }
}
