<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameters', function (Blueprint $table) {

            $table->increments('id');
            $table->string('level');
            $table->integer('time');
            $table->string('libelle');
            $table->string('validationTime');
            $table->timestamps();            
            
        });

        \App\Parameter::create([ 'time'=>90,'validationTime'=>5,'level'=>'easy', 'libelle'=>'Par défaut']);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('parameters',function (Blueprint $table) {
            
        });
    }
}
