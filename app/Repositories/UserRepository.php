<?php

namespace App\Repositories;

use App\User;

class UserRepository extends ResourceRepository {

    public function __construct(User $user) {

        $this->user = $user;
    }

     //fonction de recuperation d un client grace à son id
    public function getById($id) {
        $user = new $this->user;

       return $this->user->findOrFail($id);
   }

   //fonction update
   public function update($id,Array $inputs) {       

       $this->save($this->getById($id), $inputs);
   }


}
