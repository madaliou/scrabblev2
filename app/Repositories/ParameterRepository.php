<?php

namespace App\Repositories;

use App\Parameter;

class ParameterRepository extends ResourceRepository {

    public function __construct(Parameter $parameter) {

        $this->model = $parameter;
    }

}
