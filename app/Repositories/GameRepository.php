<?php

namespace App\Repositories;

use App\Game;

class GameRepository extends ResourceRepository {

    public function __construct(Game $game) {

        $this->model = $game;
    }

}
