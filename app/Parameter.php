<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Parameter extends Model {

    protected $table = 'parameters';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $fillable = array('time', 'level', 'libelle','validationTime');

    public function users() {

        return $this->hasMany(User::class);
    }
}
