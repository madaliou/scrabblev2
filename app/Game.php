<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Game extends Model {

    protected $table = 'games';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $fillable = array('points', 'time', 'level', 'user_id');

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function addpoints($points) {

        $this->attributes['points'] = $this->attributes['points'] + $points;
        $this->fill(array('points' => $this->attributes['points']));
        $this->save();
    }

}
