<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

//class User extends Authenticatable implements JWTSubject
class User extends Authenticatable implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Notifiable;

    protected $table = 'users';
    public $timestamps = true;

  //  use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'lastname','login','firstname', 'email', 'role', 'password', 'parameter_id'
    ]; 
    
    public function games() {

        return $this->hasMany(Game::class);
    }

    public function parameter() {
        return $this->belongsTo(Parameter::class);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
