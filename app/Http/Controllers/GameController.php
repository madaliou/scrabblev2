<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\GameRepository;

use App\Game;

use App\User;

use Auth;

use File;

use Session;

use \nspl\ds\Set;
use function \nspl\ds\set;


class GameController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $tableau = [];
    protected $userRepository;
    protected $nbrPerPage = 20;

    //injection du controleur de donnees pour appeler le repository de user
    public function __construct(GameRepository $gameRepository) {

        $this->gameRepository = $gameRepository;
        //$this->middleware('admin', ['except' => ['store', 'edit', 'update', 'destroy']]);
        //$this->middleware('auth');
    }

    /*

     * Display a listing of the resource.

     *

     * @return Response

     */

    //retourne la liste paginee des users

    public function index() {

        //if (Auth::user()->role == "admin") {

            $users = User :: all();
            $games = Game :: where('points','!=', 0)->orderBy('points', 'desc')->get();
            /*foreach($games as $game){
                print_r($game->points);
                echo '<br/>';
            }
            die();*/
            return view('pages/game/index', compact('users', 'games'));

        /*}else {

            $users = User :: where('role','!=', 'admin')->get();
            $games = Game :: where('user_id','=',Auth::user()->id)->orderBy('points', 'desc')->where('points','!=', 0)->get();
            return view('pages/game/index', compact('users', 'games'));
        }*/
    }

   

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function occurence($caractere, $tableau){
        $nb = 0;
        for($i=0;$i<sizeof($tableau);$i++){
            if($caractere == $tableau[$i]){
                $nb++;
            }
        }
        return $nb;
    }

    public function testchar($tabsaisi, $tableau){

        for($i=0;$i<sizeof($tabsaisi);$i++){

            if($this->occurence($tabsaisi[$i], $tabsaisi) > $this->occurence($tabsaisi[$i], $tableau)){               

                return "KO";

            }
            
        }

        return "OK";
    }

    public function easyview()
    {

        
        $games = \App\Game::where('points', 0)->get();
        foreach($games as $game){
            $game->delete();
        }      
        $game = Game::create([
            'level' => 'easy',
            'points' => 0,
            'time'  => '00:00',
            'user_id' => Auth::user()->id 
        ]);
      

        $filename = "liste_francais.txt";
        try
        {
            $contents = File::get($filename);
        }
        catch (Illuminate\Contracts\Filesystem\FileNotFoundException $exception)
        {
            die("The file doesn't exist");
        }       

        $content = preg_split('/\r\n|\r|\n/',  trim($contents));
        
        do{
            $rand_key = array_rand($content, 2);

            $wordeasy = trim($content[$rand_key[1]]);           

            $wordeasyshuffle = str_shuffle( $wordeasy);
            
        }while(strlen($wordeasy) > 8);

        $points = $game->points;
        
        return view('pages/game/easy',  compact('wordeasyshuffle', 'wordeasy', 'points'));
    }

    public function normalview()
    {
        $fois = count(Game::where('user_id',Auth::user()->id)->where('level','=', 'normal')->get());
        if($fois == 0){
            $game = Game::create([
                'level' => 'normal',
                'points' => 0,
                'time'  => '00:00',
                'user_id' => Auth::user()->id 
            ]);
        }else{
           $game = Game::where('user_id',Auth::user()->id)->where('level','=', 'normal')->first();
        }

        $filename = "liste_francais.txt";
        try
        {
            $contents = File::get($filename);
        }
        catch (Illuminate\Contracts\Filesystem\FileNotFoundException $exception)
        {
            die("The file doesn't exist");
        }
        $content = preg_split('/\r\n|\r|\n/',  trim($contents));

        //CHOIX NORMAL
        
        do{
            $rand_key = array_rand($content, 2);

            $wordnormal = trim($content[$rand_key[1]]);           

            $wordnormalshuffle = str_shuffle( $wordnormal);

        }while(strlen($wordnormalshuffle) <= 8 || strlen($wordnormalshuffle) > 16 );   
        
        $points = $game->points;

        return view('pages/game/normal', compact('wordnormalshuffle', 'wordnormal', 'points'));
    }

    public function hardview()
    {

        //$fois = count(Game::where('user_id',Auth::user()->id)->where('level','=', 'hard')->get());
        //if($fois == 0){
            $game = Game::create([
                'level' => 'hard',
                'points' => 0,
                'time'  => '00:00',
                'user_id' => Auth::user()->id 
            ]);
       /* }else{

            $game = Game::where('user_id',Auth::user()->id)->where('level','=', 'hard')->first();
         }*/

        $filename = "liste_francais.txt";
        try
        {
            $contents = File::get($filename);
        }
        catch (Illuminate\Contracts\Filesystem\FileNotFoundException $exception)
        {
            die("The file doesn't exist");
        }
        $content = preg_split('/\r\n|\r|\n/',  trim($contents));

        //CHOIX NORMAL
        
        do{

            $rand_key = array_rand($content, 2);

            $wordhard = trim($content[$rand_key[1]]);           

            $wordhardshuffle = str_shuffle( $wordhard);

        }while(strlen($wordhard) < 16);

        $points = $game->points;

        return view('pages/game/hard', compact('wordhardshuffle', 'wordhard', 'points'));
    }
    

    public function easy(Request $request)
    {

        $filename = "liste_francais.txt";
        try
        {
            $contents = File::get($filename, 'UTF-8');
        }
        catch (Illuminate\Contracts\Filesystem\FileNotFoundException $exception)
        {
            die("The file doesn't exist");
        }

        $answer = trim($request->reponse);

        $content = preg_split('/\r\n|\r|\n/',  trim($contents));

        $wordsplit = str_split(strtoupper($request->wordshuffle));

        for($i=0; $i<(sizeof($content)); $i++ ){

            $tcontent[$i] = strtoupper($content[$i]);
        }  

        $panswer = strtoupper($request->reponse);         

        $tanswer = str_split(strtoupper($request->reponse));

        if(Session::get('tableau')){

            $tableau = Session::get('tableau');

        }else{
            $tableau = [];
        }       
        
            
        if($this->testchar($tanswer, $wordsplit) == "OK"){

            if(in_array($answer, $content) OR in_array($panswer, $tcontent)){           

                if(!in_array($answer, $tableau)){

                $push = array_push($tableau, $answer);
                    
                $msg[] = '<br/><span class="tag tag-default tag-success font-medium-1 text-bold-100"><strong > '.$panswer.' </strong></span><br/>';       

                $arr_response = array( 'msg' => $msg, 'tab' => $this->tableau);    

                Session::put('tableau', $tableau);

                $game = Game::where('user_id', $request->auth)->where('level', '=', 'easy')->latest()->first();

                $game->addpoints(strlen($answer)); 
                
                $game->save();

                return response()->json($arr_response); 

                }else{
                    
                }
           
           
            }else{  

                if(!in_array($answer, $tableau)){

                $push = array_push($tableau, $answer);

                Session::put('tableau', $tableau);

                $msg[] = '<br/><span class="tag tag-default tag-danger font-medium-1 text-bold-100 "><strong > '.$panswer.' </strong></span><br/>';       

                $arr_response = array( 'msg' => $msg, 'tab' => $tableau); 

                return response()->json($arr_response); 

                }else{
                        
                }
            }
             
        }else {
            
            
        }

    }

    public function score(Request $request){

        $filename = "liste_francais.txt";
        try
        {
            $contents = File::get($filename);
        }
        catch (Illuminate\Contracts\Filesystem\FileNotFoundException $exception)
        {
            die("The file doesn't exist");
        }

        $content = preg_split('/\r\n|\r|\n/',  trim($contents));

        $answer = trim($request->reponse);

        $wordsplit = str_split(strtoupper($request->wordshuffle));

        for($i=0; $i<(sizeof($content)); $i++ ){

            $tcontent[$i] = strtoupper($content[$i]);
        }  

        $panswer = strtoupper($request->reponse);         

        $tanswer = str_split(strtoupper($request->reponse));


        if(Session::get('tableau')){

            $tableau = Session::get('tableau');

        }else{
            $tableau = [];
        }


        if($this->testchar($tanswer, $wordsplit) == "OK"){

            if(in_array($answer, $content) OR in_array($panswer, $tcontent)){

                if(!in_array($answer, $tableau)){

                    $push = array_push($tableau, $answer);

                    $points=$request->scores;

                    $points += strlen($request->reponse);

                    $score[] = '<span  class="font-large-2 text-bold-300 info">
                    <input id="scores" value="'.$points.'" type="hidden">'.$points.'</input> </span>';       
                    
                    $arr_response = array( 'score' => $score);

                    Session::put('tableau', $tableau);                    
            
                    return response()->json($arr_response);

                }
            }
        }
    }


    public function normal(Request $request)
    {

        $filename = "liste_francais.txt";
        try
        {
            $contents = File::get($filename);
        }
        catch (Illuminate\Contracts\Filesystem\FileNotFoundException $exception)
        {
            die("The file doesn't exist");
        }

        $answer = trim($request->reponse);

        $content = preg_split('/\r\n|\r|\n/',  trim($contents));

        $wordsplit = str_split(strtoupper($request->wordshuffle));

        for($i=0; $i<(sizeof($content)); $i++ ){

            $tcontent[$i] = strtoupper($content[$i]);
        }  

        $panswer = strtoupper($request->reponse);         

        $tanswer = str_split(strtoupper($request->reponse));

        if(Session::get('tableau')){

            $tableau = Session::get('tableau');

        }else{
            $tableau = [];
        }       
        
            
        if($this->testchar($tanswer, $wordsplit) == "OK"){

            if(in_array($answer, $content) OR in_array($panswer, $tcontent)){           

                if(!in_array($answer, $tableau)){

                $push = array_push($tableau, $answer);
                    
                $msg[] = '<br/><span class="tag tag-default tag-success font-medium-1 text-bold-100"><strong > '.$panswer.' </strong></span><br/>';       

                $arr_response = array( 'msg' => $msg, 'tab' => $tableau);    

                Session::put('tableau', $tableau);

                return response()->json($arr_response); 

                }else{
                    
                }
           
           
            }else{  

                if(!in_array($answer, $tableau)){

                $push = array_push($tableau, $answer);

                Session::put('tableau', $tableau);

                $msg[] = '<br/><span class="tag tag-default tag-danger font-medium-1 text-bold-100 "><strong > '.$panswer.' </strong></span><br/>';       

                $arr_response = array( 'msg' => $msg, 'tab' => $tableau); 

                return response()->json($arr_response); 

                }else{
                        
                }
            }
             
        }else {
            
            
        }
      
    }

    public function hard(Request $request)
    {

        $filename = "liste_francais.txt";
        try
        {
            $contents = File::get($filename);
        }
        catch (Illuminate\Contracts\Filesystem\FileNotFoundException $exception)
        {
            die("The file doesn't exist");
        }

        $answer = trim($request->reponse);

        $content = preg_split('/\r\n|\r|\n/',  trim($contents));

        $wordsplit = str_split(strtoupper($request->wordshuffle));

        for($i=0; $i<(sizeof($content)); $i++ ){

            $tcontent[$i] = strtoupper($content[$i]);
        }  

        $panswer = strtoupper($request->reponse);         

        $tanswer = str_split(strtoupper($request->reponse));

        if(Session::get('tableau')){

            $tableau = Session::get('tableau');

        }else{
            $tableau = [];
        }       
        
            
        if($this->testchar($tanswer, $wordsplit) == "OK"){

            if(in_array($answer, $content) OR in_array($panswer, $tcontent)){           

                if(!in_array($answer, $tableau)){

                $push = array_push($tableau, $answer);
                    
                $msg[] = '<br/><span class="tag tag-default tag-success font-medium-1 text-bold-100"><strong > '.$panswer.' </strong></span><br/>';       

                $arr_response = array( 'msg' => $msg, 'tab' => $this->tableau);    

                Session::put('tableau', $tableau);

                return response()->json($arr_response); 

                }else{
                    
                }
           
           
            }else{  

                if(!in_array($answer, $tableau)){

                $push = array_push($tableau, $answer);

                Session::put('tableau', $tableau);

                $msg[] = '<br/><span class="tag tag-default tag-danger font-medium-1 text-bold-100 "><strong > '.$panswer.' </strong></span><br/>';       

                $arr_response = array( 'msg' => $msg, 'tab' => $this->tableau); 

                return response()->json($arr_response); 

                }else{
                        
                }
            }
             
        }else {
            
           
        }
       
    }

    public function shuffle(Request $request){

        $filename = "liste_francais.txt";
        try
        {
            $contents = File::get($filename, 'UTF-8');
        }
        catch (Illuminate\Contracts\Filesystem\FileNotFoundException $exception)
        {

           // die("The file doesn't exist");
        }      
        $contents = mb_convert_encoding($contents, 'HTML-ENTITIES', "UTF-8");

        Session::put('tableau', []);

        $content = preg_split('/\r\n|\r|\n/',  trim(utf8_encode($contents)));

        $parameter = \App\Parameter::first();

       // $choix = $request->choice;

        $choix = $request->level;

        do{
            $rand_key = array_rand($content, 2);

            $word = trim($content[$rand_key[1]]);           

            $wordshuffle = str_shuffle($word);
            
        }while(strlen($word) <3 || strlen($word) > 8);

        /*switch ($choix) {
            case "easy":
            do{
                $rand_key = array_rand($content, 2);
    
                $word = trim($content[$rand_key[1]]);           
    
                $wordshuffle = str_shuffle( $word);
                
            }while(strlen($word) > 8);
                break;
            case "normal":
            do{
                $rand_key = array_rand($content, 2);
    
                $word = trim($content[$rand_key[1]]);           
    
                $wordshuffle = str_shuffle( $word);
                
            }while(strlen($word) > 8);
                break;
            case "hard":
            do{
                $rand_key = array_rand($content, 2);
    
                $word = trim($content[$rand_key[1]]);           
    
                $wordshuffle = str_shuffle( $word);
                
            }while(strlen($word) > 8);
                break;
        }*/

        return response()->json(['success' => true, 'message' => "OK",'wordshuffle' => $wordshuffle]);
    
        /*$debut = '<br/>  <span class="font-large-2 text-bold-300 info">';

       for($i=0;$i<strlen($wordshuffle);$i++){

        //mb_convert_encoding($data['name'], 'UTF-8', 'UTF-8')

            $milieu[$i] = strtoupper(mb_convert_encoding($wordshuffle[$i], 'UTF-8', 'UTF-8')). ' ';
        }

        $fin[] =' </span>';

        $propo[] = '';

        $ar_response = array('msg' => $milieu, 'wordshuffle' => $wordshuffle, 'propo' => $propo);

        return response()->json($ar_response, 200, ['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);                   
              */
    }


    //New functions

    public function store(Request $request)
    { 

        $games = \App\Game::where('points', 0)->where('user_id', Auth::user()->id)->get();
        
        foreach($games as $game){
            $game->delete();
        } 

        $parameter = \App\Parameter::first();

        $game = Game::create([
            'level' =>  $parameter->level,
            'points' => 0,
            'time'  => $parameter->time,
            'user_id' => Auth::user()->id 
        ]);             


        return response()->json(['success' => true, 'message' => "Jeu ajouté avec succès",
        'game' => $game
        ]);
                
        
    }

    public function list() {
        
        $users = User::with('games')->get();

        //$games = Game::where('points','!=', 0)->with('user')->orderBy('points', 'desc')->paginate(1000);

        $games = Game::with('user')->orderBy('points', 'desc')->paginate(1000);


        return response()->json($games);            

    }

    public function searchInGamesList(Request $request)
    {        
        # code...
        $occurence = $request->input('occurence');

        $games = Game::whereHas('user', function ($query) use ($request) {
            $query->where('login', 'like', "%{$request->occurence}%")
            ->orwhere('email', 'like', "%{$request->occurence}%")
            ->orwhere('firstname', 'like', "%{$request->occurence}%")
            ->orwhere('lastname', 'like', "%{$request->occurence}%");
        })->with('user')->orderBy('points', 'desc')->paginate(1000);
         
        return response()->json($games);
    }

    public function edit(Request $request, $id)
    {

        $user = User::where('id', $id)->first();

                if ($user) {

                    //Modification des infos de l'utilisateur
                    $firstname = $request->input('firstname');
                    $lastname = $request->input('lastname');
                    $login = $request->input('login');
                    $email = $request->email;

                   
                    $role = $request->input('role');       

                    
                        if($user) {

                             /* if($user->email != $email OR $user->firstname!= $firstname OR $user->lastname != $lastname OR  $user->login != $login){                        

                                $verifemail = User::where('email', $email)->first();

                                if ($verifemail) {

                                  return response()->json(['success' => 0, 'message' => "Cet email est déjà enregistré"]);
                                
                                } else {
                                    
                                    if (!filter_var ($email, FILTER_VALIDATE_EMAIL)){
                                        return response()->json(['success' => 0, 'message' => "Cet email n'est pas valide"]);
                                   }else {*/
                                
                                    $user->email  = $email ? $email : $user->email;
                                    $user->firstname = $firstname;
                                    $user->lastname = $lastname ;
                                    $user->login = $login;
                                    $user->role = $role;
                                    $user->save();
                                    
                                    return response()->json([
                                        'success' => true, 
                                        'message' => "User edited sucessfully!",
                                        'user' => $user ]);
                                    
                                     

                        }else{
                            return response()->json(['success' => 0, 'message' => "this user does not exist"]);
                            }
                }

            }


    //New api for word validation

    public function easyWordValidation(Request $request, $id)
    {

        $filename = "liste_francais.txt";
        try
        {
            $contents = File::get($filename, 'UTF-8');
        }
        catch (Illuminate\Contracts\Filesystem\FileNotFoundException $exception)
        {
            die("The file doesn't exist");
        }

        $answer = trim($request->reponse);

        $content = preg_split('/\r\n|\r|\n/',  trim($contents));

        $wordsplit = str_split(strtoupper($request->wordshuffle));

        for($i=0; $i<(sizeof($content)); $i++ ){

            $tcontent[$i] = strtoupper($content[$i]);
        }  

        $panswer = strtoupper($request->reponse);         

        $tanswer = str_split(strtoupper($request->reponse));

        
        
        if(Session::get('tableau')){

            $tableau = Session::get('tableau');
            

        }else{

            $tableau = [];
        }       
            
        if($this->testchar($tanswer, $wordsplit) == "OK"){

            if(in_array($answer, $content) OR in_array($panswer, $tcontent)){     
                
                if(!in_array($answer, $tableau)){

                    $push = array_push($tableau, $answer);

                    Session::put('tableau', $tableau);

                    //return Session::get('tableau');

                    $level = $request->level;

                    $parameter = Auth::user()->parameter;

                    switch ($level) {
                        case 'easy':
                            # code...
                            $game = Game::where('id', $id)->first();

                            $game->addpoints(strlen($answer)); 
                            
                            $game->save();
                        
                            return response()->json([
                                'success' => true, 
                                'message' => "OK",
                                'points' => $game->points,
                                'parameter' => $parameter ], 200);
                            break;
                        case 'normal':
                            # code...
                            if(strlen($answer)>2){
                                $game = Game::where('id', $id)->first();

                                $game->addpoints(strlen($answer)); 
                                
                                $game->save();
                        
                                return response()->json([
                                    'success' => true, 
                                    'message' => "OK",
                                    'points' => $game->points,
                                    'parameter' => $parameter ], 200);
                                }else{

                                    return response()->json([
                                        'success' => false, 
                                        'message' => "NOK" ]);
                                }
                            break;
                        case 'hard':
                            # code...
                            if(strlen($answer)>3){

                                $game = Game::where('id', $id)->first();

                                $game->addpoints(strlen($answer)); 
                                
                                $game->save();
                        
                                return response()->json([
                                    'success' => true, 
                                    'message' => "OK",
                                    'points' => $game->points,
                                    'parameter' => $parameter ], 200);
                                }else{

                                    return response()->json([
                                        'success' => false, 
                                        'message' => "NOK" ]);
                                }
                            break;
                        
                        default:
                            # code...
                            break;
                    }

                    /*$push = array_push($tableau, $answer);
                        
                    $msg[] = '<br/><span class="tag tag-default tag-success font-medium-1 text-bold-100"><strong > '.$panswer.' </strong></span><br/>';       

                    $arr_response = array( 'msg' => $msg, 'tab' => $this->tableau);    

                    Session::put('tableau', $tableau);
                    
                    return response()->json($arr_response); 

                    */

                                    

                }else{

                    return response()->json([
                        'success' => false, 
                        'message' => "NOK" ]);  
                }
           
           
            }else{  

                if(!in_array($answer, $tableau)){

                $push = array_push($tableau, $answer);

                /*Session::put('tableau', $tableau);

                $msg[] = '<br/><span class="tag tag-default tag-danger font-medium-1 text-bold-100 "><strong > '.$panswer.' </strong></span><br/>';       

                $arr_response = array( 'msg' => $msg, 'tab' => $tableau); */

               // return response()->json($arr_response); 

               return response()->json([
                'success' => false, 
                'message' => "NOK" ]);  

                }else{
                    return response()->json([
                        'success' => false, 
                        'message' => "NOK" ]);  
        
                }
            }
             
        }else {
            
            return response()->json([
                'success' => false, 
                'message' => "NOK" ]);  

            
        }

    }
                    
        

}
