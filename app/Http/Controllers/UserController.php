<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Repositories\UserRepository;
use App\Http\Requests\RegisterAuthRequest;
use App\Http\Requests\UserUpdateRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use URL;
use DB;
use JWTAuth;
use JWTAuthException;
use Auth;
use Excel;
use Session;



use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;


class UserController extends Controller
{
    
    protected $userRepository;
    //protected $user;
    
    
    public function __construct(UserRepository $userRepository)
    {

        $this->userRepository = $userRepository;
        //$this->user = JWTAuth::parseToken()->authenticate();

             

    }

    public function login3(Request $request)
    {
        //$email = $request->email;
        $login = $request->login;

        try {
            //code...
            $user = User::where('login', $login)
            //->orWhere('login', $login)
            ->first();
       
        

        $token = null;
        if (!$token = JWTAuth::fromUser($user)) {
        return $this->respondInternalError( 'Can\'t generate the JWT token right now, try again later!', 'object', 400);
        }
        $tokenData = $this->respondWithToken($token);

        return response()->json([
            'success' => true,
            'tokenData' => $tokenData,
            'user' => $user
        ]);

    } catch (\Throwable $th) {

        return response()->json([
            'success' => false,
            'message' => 'Invalid Email or Password',
        ], Response::HTTP_BAD_REQUEST);
    }
        /*
        $input = $request->only('email', 'password');
        $input2 = $request->only('login', 'password');
        $jwt_token = null;
 
        if (!$jwt_token = JWTAuth::attempt($input)) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Email or Password',
            ], 401);
        }
        if (!$jwt_token = JWTAuth::attempt($input2)){
            return response()->json([
                'success' => false,
                'message' => 'Invalid Email or Password',
            ], 401);
        }
        $user = Auth::user();

        $token = $this->respondWithToken($jwt_token);
 
        return response()->json([
            'success' => true,
            'tokenData' => $token,
            'user' => $user
        ]);*/
    }

    public function login(Request $request)
    {

        $input = $request->only('login', 'password');
        $login = $request->login;
        $jwt_token = null;
        
        if (!$jwt_token = JWTAuth::attempt($input)) {
            return response()->json([
                'success' => false,
                'message' => 'Identifiant ou mot de passe incorrect',
            ], 401);
        }
        $tokenData = $this->respondWithToken($jwt_token);
        $user = User::where('login', $login)->first();
 
        return response()->json([
            'success' => true,
            //'token' => $jwt_token,
            'tokenData' => $tokenData,
            'user' => $user
        ]);
    }

  
 
    public function register(RegisterAuthRequest $request)
    {

        $firstname = $request->firstname;
        $lastname = $request->lastname;
        $phone = $request->phone;
        $email = $request->email;
        $role = $request->role;
        $user->login = $login;
        $password = bcrypt($request->password);

        if($firstname == "" OR $lastname == ""){
            return response()->json(['success' => 0, 'message' => "Firstname or Lastname not set"]);
        }else{

            if ($email && !filter_var ($email, FILTER_VALIDATE_EMAIL)){
                return response()->json(['success' => 0, 'message' => "Cet email n'est pas valide"]);
            }else{

                                   
                    $phone = str_replace(CHR(32), "", $phone);
        
                    switch (strlen($phone)) {
                        case '8':
            
                            $phone = '228' . $phone;
            
                            break;
                        case '11':
            
                            if (substr($phone, 0, 3) == '228') {
                                $phone = '228' . substr($phone, -8);
                            } else {
                                return response()->json(['success' => 0, 'message' => "Ce numéro de téléphone est incorrect, Veuillez réessayer avec le format XXXXXXXX ."]);
                            }
                            break;
            
                        default:
                            return response()->json(['success' => 0, 'message' => "Ce numéro de téléphone est incorrect, Veuillez réessayer avec le format XXXXXXXX ."]);
                            break;
                    }
                
                            

                if($phone!="" && User::where('phonenumber', $phone)->count() > 0 ){
                    
                    return response()->json(['success' => 0, 'message' => "Ce numero de téléphone est deja utilisé"]);
                    
                }elseif(($email!="" && User::where('email', $email)->count() > 0) ){
            
                    return response()->json(['success' => 0, 'message' => "Cette adresse email est déjà utilisée"]);
        
                }else {

                    $user = new User();
                    $user->firstname = $firstname;
                    $user->lastname = $lastname;
                    $user->phonenumber = $phonenumber;
                    $user->role = 'user';
                    $user->email = $email;
                    $user->login = $login;
                    $user->password = $password;
                    $user->save();

                return response()->json(['success' => 1, 'message' => "Utilisateur créé avec succès"]);
            
            }
            
            }

        }
        return response()->json([
            'success' => true,
            'data' => $user
        ], 200);

    }
 
    
 
    public function logout(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);
 
        try {
            JWTAuth::invalidate($request->token);
 
            return response()->json([
                'success' => true,
                'message' => 'User logged out successfully'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, the user cannot be logged out'
            ], 500);
        }
    }
 
    public function getAuthUser(Request $request)
    {
 
        //$user = JWTAuth::authenticate($request->token);

        //$user = Auth::user()->with('games')->get();

        $user = Auth::user();
 
        return response()->json(['user'  => $user]);

    }

    public function getAuthUserGames(Request $request)
    {
 
        //$user = JWTAuth::authenticate($request->token);
       
        try {
            //code...

            $user = Auth::user();

            $mygames = $user->games->where('points', '!=', 0)->orderBy('points', 'desc')->paginate(1000);

            return response()->json(['games' => $mygames]);

        } catch (\Throwable $th) {
            //throw $th;
            return response()->json(['success' => false, 'message' => "Une erreur est survenue!!"]);
        }
 
        //$games = \App\Game::where('user_id', $user->id)->where('points', '!=', 0);
        

    }
   
    public function create(Request $request){ 

        $firstname = $request->input('firstname');

        $lastname = $request->input('lastname');                
        
        if($firstname != "" && $lastname != ""){               
                        
            $request['firstname'] = strtoupper($firstname);
            $request['lastname'] = strtoupper($lastname);
            $email = $request->input('email');
            $role = $request->input('role'); 
            $login = $request->input('login');     
            
            if ($email && !filter_var ($email, FILTER_VALIDATE_EMAIL)){
                return response()->json(['success' => 0, 'message' => "Cet email n'est pas valide"]);
            }else{                

                $passe = bcrypt($request->input('password'));

                    if(($email!="" && User::where('email', $email)->count() > 0) ){
                
                        return response()->json(['success' => false, 'message' => "Cette adresse email est déjà utilisée"]);
            
                    }else if(($login!="" && User::where('login', $login)->count() > 0) ){

                        return response()->json(['success' => false, 'message' => "Ce login est déjà utilisé"]);
                    
                    }else {

                        $user = User::create([

                            'email' => $email,
                            'role' => $role,
                            'firstname' => $firstname,
                            'lastname' => $lastname, 
                            'login' => $login,       
                            'password' => $passe
                                
                        ]);

                        return response()->json(['success' => true, 'message' => "Utilisateur créé avec succès",
                        'user' => $user
                        ]);
                    
                    }
            }
        
        }else{
            return response()->json(['success' => 0,
                'message' => "Attention, Vérifiez si le nom et le prénom sont renseignés"
                ]);
        }       
        
    }


    public function list() {
        
        $users = User::with('games')->with('parameter')->orderBy('id', 'desc')->paginate(1000);

        return response()->json($users);            

    }

    public function edit(Request $request, $id)
    {
        
        $user = User::where('id', $id)->first();

                if ($user) {

                    //Modification des infos de l'utilisateur
                    $firstname = $request->input('firstname');
                    $lastname = $request->input('lastname');
                    $email = $request->email;
                    $password = $request->password;                   
                    $role = $request->input('role'); 
                    $login = $request->input('login');       
  
                    $user->email  = $email ? $email : $user->email;
                    $user->firstname = strtoupper($firstname);
                    $user->lastname = strtoupper($lastname) ;
                    $user->password = bcrypt($password);
                    $user->role = $role;
                    $user->login = $login;
                    $user->save();
                    
                    return response()->json([
                        'success' => true, 
                        'message' => "Utilisateur modifié avec succès!",
                        'user' => $user ], 200);
        }else{
            return response()->json(['success' => false, 'message' => "Cet utilisateur n'existe pas"]);
            }
                
 }

 public function destroy($id){
     
        $user = \App\User::find($id);
        if($user){
            $user->delete();
            return response()->json(['success' => true, 'message' => "Utilisateur supprimé avec succès"]);
        }else{
            
            return response()->json(['success' => false, 'message' => "Cet utilisateur n'existe pas"]);
        }
        
    }
             
    
    public function createByExcel(Request $request)
    {
        $listeexcel = $request->file('liste');

        Session::put('status','');

        Excel::load($listeexcel, function($reader)  {

            $results = $reader->get();

            $headerRow = $reader->first()->keys()->toArray();
            $rows = $reader->toArray();
            $totalRows = count($rows);
           
                foreach ($reader->toArray() as $row) {

                    $email = $row['email'];
                    $lastname = $row['nom'];
                    $firstname = $row['prenom']; 
                    $login = $row['login']; 

                    $email = str_replace(CHR(32), "", $email);

                    $user = User::where('email', $email)->first();

                    if ($user) {

                        $ok = Session::get('status');

                                if (!$ok == 'ok') {
                                    # code...
                                    Session::put('status','ok');
                                }
                    } else  {

                        $password = $row['password'];                                          
                        
                       
                            $id = Session::get('auteur_id');
                               
                            $utilisateur = User::create([
                                        'email' => $email,
                                        'firstname' => $firstname,
                                        'lastname' => $lastname,
                                        'login' => $login, 
                                        'role' => 'user',                                            
                                        'password' => bcrypt($password),
                            ]);
                            
                            $ok = Session::get('status');
                            
                            if (!$ok == 'ok') {
                                # code...
                                Session::put('status','ok');
                            }
                        }
                    }

        });

        $status = Session::get('status');

        //CHECK VARIABLE VALUE FOR DECISION
        if ($status == 'error') {
                /*Alert::error("Attention Vous n'avez pas assez de ".Auth::user()->moozi_name()." pour crediter la liste")->persistent("Ok");
                return redirect()->back();
        }else if ($status == 'interneterror') { 
            Alert::error("Attention Veullez verifier votre connexion internet")->persistent("Ok");
                return redirect()->back();
             
        }else if ($status == 'smserror') { 
            return response()->json(['success' => false, 'message' => "Problème de connexion internet"]);
             */
            return response()->json(['success' => false, 'message' => "Une erreur est survenue"]);
        }else{
            return response()->json(['success' => true, 'message' => "Opération effectuée avec succès"]);
        }

    }

    public function insert()
    {

        for($i=0; $i<20000; $i++){

            $user = User::create([
                'email' => 'cigc'.$i.'@gmail.com',
                'firstname' => 'toto'.$i,
                'lastname' => 'tata'.$i, 
                'role' => 'user',                                            
                'password' => bcrypt('azerty'),
            ]);
    
        }
        
        $users = User::with('games')->get();

        return response()->json(['success' => true, 'message' => "Succès", 'users' => $users]);
        
        //return response()->json($users);            

    }


    public function searchInUsersList(Request $request)
    {        
        # code...
        $occurence = $request->occurence;

        $users = User::where('login', 'like', '%'.$occurence . '%')
        ->orwhere('login', 'like', '%'.$occurence . '%')
        ->orwhere('email', 'like', '%'.$occurence . '%')
        ->orwhere('firstname', 'like', '%'.$occurence . '%') 
        ->orwhere('lastname', 'like', '%'.$occurence . '%')
        ->orderBy('firstname')
        ->paginate(1000); 
         
        return response()->json($users);
    }
        
}