<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Parameter;
use App\User;
use App\Repositories\ParameterRepository;
use App\Repositories\UserRepository;
use App\Http\Requests\RegisterAuthRequest;
use App\Http\Requests\UserUpdateRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use URL;
use DB;
use JWTAuth;
use JWTAuthException;
use Auth;
use Excel;
use Session;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;


class ParameterController extends Controller
{
    
    protected $parameterRepository;
    //protected $parameter;
    
    
    public function __construct(ParameterRepository $parameterRepository, UserRepository  $userRepository)
    {

        $this->parameterRepository = $parameterRepository;

        $this->userRepository = $userRepository;             

    }  

    
    public function list()

    {
        
        $parameters = Parameter::with('users')->get();

        return response()->json($parameters);            

    }


    public function create(Request $request)
    {        
        $parameter = new Parameter;

        $parameter->time = $request->time;

        $parameter->validationTime = $request->validationTime;

        $parameter->level = $request->level;

        $parameter->libelle = $request->libelle;

        $parameter->save();

        $posts = $request->users;

        foreach($posts as $post){

            $user = $this->userRepository->getById($post);

            $user->parameter_id = $parameter->id;

            $user->save();

        }        

        return response()->json(['success' => true, 'message' => "Succès"]);

    }


    public function oneParameter($id){
        
        $user = \App\User::where('id', $id)->first();

        $parameter = Parameter::where('id', $user->parameter_id)->first();

        return response()->json($parameter);
    }


    public function edit(Request $request, $id)
    {        

        $parameter = Parameter::where('id', $id)->first();

        if ($parameter) {

            
            $parameter->time = $request->input('time');
            $parameter->level = $request->input('level');
            $parameter->libelle = $request->input('libelle');
            $parameter->validationTime = $request->input('validationTime');

            $parameter->save();

            $users = \App\User::where('parameter_id', $id)->get();

            foreach($users as $user){
                $user->parameter_id = 1;
                $user->save();
            }

            $posts = $request->users;

            foreach($posts as $post){

                $user = $this->userRepository->getById($post);

                $user->parameter_id = $parameter->id;

                $user->save();

            }           
                        
            return response()->json([
                'success' => true, 
                'message' => "Parametrage modififié avec succès!",
                'parameter' => $parameter ], 200);
                    
        }else{
            return response()->json(['success' => 0, 'message' => "Ce parametrage n'existe pas"], 401);
            }            
    }

    public function destroy($id){
     
        $parameter = \App\Parameter::find($id);

        if($parameter->id == 1){
            return response()->json(['success' => 0, 'message' => "Ce parametrage ne peut pas etre supprimé"], 401);
        }else{

            $users = \App\User::where('parameter_id', $parameter->id)->get();

        foreach($users as $user){

            $user->parameter_id = 1;

            $user->save();            

        }

        if($parameter){
            
            $parameter->delete();
            return response()->json(['success' => true, 'message' => "Parametrage supprimé avec succès"]);
        }else{
            
            return response()->json(['success' => false, 'message' => "Cet parametrage n'existe pas"]);
        }

        }
        
        
        
    }
        
        
}