<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DiaspotestimonialCreateRequest extends FormRequest {

    /**
     * Determine if the Testimonial is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'author_name' => 'bail|required',
            'author_location' => 'bail|required',
            'author_age' => 'bail|required',
            'author_message' => 'bail|required'
        ];
    }

}
